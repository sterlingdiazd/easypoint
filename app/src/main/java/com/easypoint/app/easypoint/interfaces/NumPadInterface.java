package com.easypoint.app.easypoint.interfaces;

/**
 * Created by sterlingdiazd on 10/03/2015.
 */
public interface NumPadInterface
{

	/**
	 * Request process ins initiated, and the caller is notified throught this method
	 */
	void onAddNumber(Object oject);


	void onBackSpace();

}
