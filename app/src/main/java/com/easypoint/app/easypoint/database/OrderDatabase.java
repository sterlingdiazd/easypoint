package com.easypoint.app.easypoint.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.easypoint.app.easypoint.model.Order;
import com.easypoint.app.easypoint.model.OrderDetail;

import java.util.ArrayList;
import java.util.List;


public class OrderDatabase extends SQLiteOpenHelper
{

	public static final  String TABLE_NAME_ORDER                 = "OrderSales";
	public static final  String COLUMN_NAME_ORDER_ID_ORDER       = "idOrder";
	public static final  String COLUMN_NAME_ORDER_ID_CLIENT      = "idClient";
	public static final  String COLUMN_NAME_ORDER_ID_EMPLOYEE    = "idEmployee";
	public static final  String COLUMN_NAME_ORDER_PAYMENT_METHOD = "paymentMethod";
	public static final  String COLUMN_NAME_ORDER_DATETIME       = "orderDateTime";
	private static final int    DATABASE_VERSION                 = 2;
	private Context context;

	private String createTable_order = "create table " +
	                                   TABLE_NAME_ORDER + " ( " +
	                                   COLUMN_NAME_ORDER_ID_ORDER + " integer primary key autoincrement , " +
	                                   COLUMN_NAME_ORDER_ID_CLIENT + " TEXT , " +
	                                   COLUMN_NAME_ORDER_ID_EMPLOYEE + " TEXT , " +
	                                   COLUMN_NAME_ORDER_PAYMENT_METHOD + " TEXT , " +
	                                   COLUMN_NAME_ORDER_DATETIME + " TEXT  )";

	public OrderDatabase(Context context)
	{
        super(context, TABLE_NAME_ORDER, null, DATABASE_VERSION);
        if(context != null)
        {
            this.context = context;
        }

	}

	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL(createTable_order);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ORDER);
		db.execSQL(createTable_order);
	}

	public long addOrder(Order order)
	{
        long idOrder = -1;
        try
        {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_NAME_ORDER_ID_CLIENT, order.getIdClient());
            contentValues.put(COLUMN_NAME_ORDER_ID_EMPLOYEE, order.getIdEmployee());
            contentValues.put(COLUMN_NAME_ORDER_PAYMENT_METHOD, order.getPaymentMethod());
            contentValues.put(COLUMN_NAME_ORDER_DATETIME, order.getOrderDateTime());
            idOrder = db.insert(TABLE_NAME_ORDER, null, contentValues);
            db.close();

        } catch (Exception e)
        {

            e.printStackTrace();
        }
        return idOrder;
	}
	
	public int updateOrder(Order order)
	{
		SQLiteDatabase db = null;
		db = getWritableDatabase();
		ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME_ORDER_ID_ORDER, order.getIdOrder());
        contentValues.put(COLUMN_NAME_ORDER_ID_CLIENT, order.getIdClient());
        contentValues.put(COLUMN_NAME_ORDER_ID_EMPLOYEE, order.getIdEmployee());
        contentValues.put(COLUMN_NAME_ORDER_PAYMENT_METHOD, order.getPaymentMethod());
        contentValues.put(COLUMN_NAME_ORDER_DATETIME, order.getOrderDateTime());
		int ret = db.update(TABLE_NAME_ORDER, contentValues, COLUMN_NAME_ORDER_ID_ORDER + " = " + order.getIdOrder(), null);
		db.close();
		return ret;
	}


    public Order getOrderByID(String id)
    {
        SQLiteDatabase db = getWritableDatabase();
        String[] proyection =  new String[]
                {
                        COLUMN_NAME_ORDER_ID_ORDER,
                        COLUMN_NAME_ORDER_ID_CLIENT,
                        COLUMN_NAME_ORDER_ID_EMPLOYEE,
                        COLUMN_NAME_ORDER_PAYMENT_METHOD,
                        COLUMN_NAME_ORDER_DATETIME
                };
        Order order = null;
        try
        {
            Cursor cursor = db.query(TABLE_NAME_ORDER, proyection, COLUMN_NAME_ORDER_ID_ORDER + " = ?", new String[]{id}, null, null, COLUMN_NAME_ORDER_ID_ORDER);

            if(cursor != null && cursor.getCount() != 0)
            {
                if(cursor.moveToFirst())
                {
                    do
                    {
                        String idOrder = cursor.getString(0);
                        String idClient = cursor.getString(1);
                        String idEmployee = cursor.getString(2);
                        String paymentMethod = cursor.getString(3);
                        String dateTime = cursor.getString(4);

                        List<OrderDetail> orderDetails = new OrderDetailDatabase(context).getOrderDetailsByOrderID(idOrder);
                        order = new Order(idOrder, idClient, idEmployee, dateTime, paymentMethod, orderDetails);
                    }
                    while(cursor.moveToNext());
                }
            }
            cursor.close();
            db.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return order;
    }

    public List<Order> getOrdesrByClient(String clientID)
    {
        SQLiteDatabase db = getWritableDatabase();
        String[] proyection =  new String[]
                {
                        COLUMN_NAME_ORDER_ID_ORDER,
                        COLUMN_NAME_ORDER_ID_CLIENT,
                        COLUMN_NAME_ORDER_ID_EMPLOYEE,
                        COLUMN_NAME_ORDER_PAYMENT_METHOD,
                        COLUMN_NAME_ORDER_DATETIME
                };
        List<Order> orders = new ArrayList<Order>();
        try
        {
            Cursor cursor = db.query(TABLE_NAME_ORDER, proyection, COLUMN_NAME_ORDER_ID_CLIENT + " = ?", new String[]{clientID}, null, null, COLUMN_NAME_ORDER_ID_ORDER);

            if(cursor != null && cursor.getCount() != 0)
            {
                if(cursor.moveToFirst())
                {
                    do
                    {
                        String idOrder = cursor.getString(0);
                        String idClient = cursor.getString(1);
                        String idEmployee = cursor.getString(2);
                        String paymentMethod = cursor.getString(3);
                        String dateTime = cursor.getString(4);

                        List<OrderDetail> orderDetails = new OrderDetailDatabase(context).getOrderDetailsByOrderID(idOrder);
                        orders.add( new Order(idOrder, idClient, idEmployee, dateTime, paymentMethod, orderDetails) );
                    }
                    while(cursor.moveToNext());
                }
            }
            cursor.close();
            db.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return orders;
    }

	public List<Order> getAllOrders()
    {
        SQLiteDatabase db = getWritableDatabase();
        String[] proyection =  new String[]
                {
                        COLUMN_NAME_ORDER_ID_ORDER,
                        COLUMN_NAME_ORDER_ID_CLIENT,
                        COLUMN_NAME_ORDER_ID_EMPLOYEE,
                        COLUMN_NAME_ORDER_PAYMENT_METHOD,
                        COLUMN_NAME_ORDER_DATETIME
                };
        List<Order> orders = new ArrayList<Order>();
        try
        {
            Cursor cursor = db.query(TABLE_NAME_ORDER, proyection, null, null, null, null, COLUMN_NAME_ORDER_ID_ORDER);

            if(cursor != null && cursor.getCount() != 0)
            {
                if(cursor.moveToFirst())
                {
                    do
                    {
                        String idOrder = cursor.getString(0);
                        String idClient = cursor.getString(1);
                        String idEmployee = cursor.getString(2);
                        String paymentMethod = cursor.getString(3);
                        String dateTime = cursor.getString(4);

                        List<OrderDetail> orderDetails = new OrderDetailDatabase(context).getOrderDetailsByOrderID(idOrder);
                        orders.add( new Order(idOrder, idClient, idEmployee, dateTime, paymentMethod, orderDetails) );

                    }
                    while(cursor.moveToNext());
                }
            }
            cursor.close();
            db.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return orders;
	}

	public int deleteOrder(int idOrder){
		SQLiteDatabase db = null;
		db = getWritableDatabase();
		int ret = db.delete(TABLE_NAME_ORDER, COLUMN_NAME_ORDER_ID_ORDER + " = " + idOrder,  null );
		db.close();
		return ret;
	}

}
