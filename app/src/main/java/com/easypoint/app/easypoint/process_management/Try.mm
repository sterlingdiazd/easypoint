//KMF_VERSION=4.18.3
//VERSION=1.0.0-SNAPSHOT
class sample.Cloud {
    nodes : sample.Node[0,*]
}
class sample.Node {
    name : String
    softwares : sample.Software[0,*]
}
class sample.Software {
    name : String
    size : Int
}
