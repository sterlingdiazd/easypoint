package com.easypoint.app.easypoint.util;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import com.easypoint.app.easypoint.database.ProductDatabase;
import com.easypoint.app.easypoint.model.Product;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;

/**
 * Created by sterlingdiazd on 6/21/2014.
 */
public class ProductImageManager
{
	private static int defaultUdLenght = 5;

	public static String generateImageName(Product product)
	{
		String imageName = fillIdZeroPad(product.getId(), defaultUdLenght) + "-" + optimizeStringToCode(product.getDescription());
		return imageName;
	}

	private static String optimizeStringToCode(String description)
	{
		return description.toLowerCase().replace(" ", "_");
	}

	private static String fillIdZeroPad(String productID, int defaultIdLenght)
	{
		String newID       = "";
		int    idLenght    = productID.length(); //2
		int    zerosNeeded = defaultIdLenght - idLenght; //3
		for (int x = 0; x < zerosNeeded; x++)
        {
            if (idLenght < defaultIdLenght)
            {
                newID += "0";
            }
        }
        newID += productID;
        return newID;
    }


	public static String generateImagePath(Activity activity, Product product, Uri selectedImageUri)
	{
		//generar el nombre de la imagen
		String imageName = ProductImageManager.generateImageName(product);
		String fullNameOrUri;
		String  imageFormat = "";
		if(selectedImageUri != null) //Si cambio la imagen, y se seteo el imageUri, entonces el formato se tomará del uri, y se le pondra al imagePath o uri de la imagen
		{
			HashMap properties  = ProductImageManager.getFileProperties(activity, selectedImageUri);
			String  mimyType    = properties.get("format").toString();

			if (mimyType.equals("image/jpeg"))
			{
				imageFormat = ".jpg";
			}
			else if (mimyType.equals("image/png"))
			{
				imageFormat = ".png";
			}
		}
		else
		{

			imageFormat = extractFormatFromFileName(product.getImagePath());
		}

		fullNameOrUri = imageName + imageFormat;
		return fullNameOrUri;
	}

	private static String extractFormatFromFileName(String imageName)
	{
		String [] imageNameComponents = imageName.split("\\.");
		String format = "";
		if(imageNameComponents.length == 2 && imageNameComponents[1] != null)
		{
			format = "." + imageNameComponents[1];
		}
		return format;
	}


	public static HashMap getFileProperties(Activity activity, Uri uri)
	{
		String[] projection = {MediaStore.MediaColumns.DATA,  MediaStore.MediaColumns.MIME_TYPE};
		Cursor   cursor     = activity.getContentResolver().query(uri, projection, null, null, null);
		if (cursor != null)
		{
			//HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
			//THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
			cursor.moveToFirst();
			int columnIndexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
			int columnIndexFormat = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.MIME_TYPE);
			String filePath = cursor.getString(columnIndexData);
			String format = cursor.getString(columnIndexFormat);

			HashMap properties = new HashMap<String, String>();
			if(format.length() > 0 && (format.equalsIgnoreCase("image/jpeg") || format.equalsIgnoreCase("image/jpg") || format.equalsIgnoreCase("image/png") ) )
			{

				properties.put("format", format);
				properties.put("path", filePath);
			}

			cursor.close();
			return properties;
		}
		else
		{
			return null;               // FOR OI/ASTRO/Dropbox etc
		}
	}

	public static boolean saveToSDCard(Activity activity, Product product)
	{
		boolean successfullOperation = false;
		if (product.getBitmap() != null)
		{
			String path = getProductImageDirectory( product.getImagePath());
			File file = new File(path);
			if (!file.exists())
			{
				try
				{
					FileOutputStream out = new FileOutputStream(file);
					product.getBitmap().compress(Bitmap.CompressFormat.PNG, 90, out);
					out.flush();
					out.close();
					successfullOperation = true;
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			} else {
				successfullOperation = true;
			}
		}
		return successfullOperation;
	}

	public static boolean renameFile(String oldName, String newName)
	{
		File oldFile = new File( getProductImageDirectory( oldName) );
		File newFile = new File( getProductImageDirectory( newName) );

		if(oldFile.exists()){
			if(oldFile.exists())
				return oldFile.renameTo(newFile);
		}
		return false;
	}

	public static String getProductImageDirectory(String imageName)
	{
		String path = Configuration.FILES_DIR +"/" + Configuration.IMAGE_DIRECTORY + "/" + imageName;
		return path;
	}
}
