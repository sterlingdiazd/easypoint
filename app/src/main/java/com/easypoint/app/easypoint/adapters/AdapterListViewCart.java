package com.easypoint.app.easypoint.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.model.Product;
import com.easypoint.app.easypoint.util.Configuration;

import java.util.List;


public class AdapterListViewCart extends BaseAdapter
{

	private Activity      activity;
	private List<Product> products;


	public AdapterListViewCart(Activity activity, List<Product> products)
	{
		this.activity = activity;
		this.products = products;
		Configuration configuration = Configuration.getInstance(activity);
        configuration.configureSharePreference();
	}

	public View getView(int position, View view, ViewGroup viewGroup)
	{
		LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View product_item = layoutInflater.inflate(R.layout.layout_product_item, null);

		TextView textViewProductCant = (TextView) product_item.findViewById(R.id.textViewProductCant);
        TextView textViewProductName = (TextView) product_item.findViewById(R.id.textViewProductName);
        TextView textViewProductPrice = (TextView) product_item.findViewById(R.id.textViewProductPrice);
        TextView textViewProductSubTotal = (TextView) product_item.findViewById(R.id.textViewProductSubTotal);

        Product product = products.get(position);

        String quantity =  product.getQuantity();
        String price = product.getPrice();

        textViewProductCant.setText( quantity );
        textViewProductName.setText( product.getDescription());
        textViewProductPrice.setText( price );
        textViewProductSubTotal.setText( (Integer.valueOf(quantity) * Integer.valueOf(price)) + "" );

		return product_item;
	}

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getCount() {
        return products.size();
    }

    public Object getItem(int position) {
        return products.get(position);
    }

    public long getItemId(int id) {
        return id;
    }
	

	
}
