package com.easypoint.app.easypoint.model;

import com.easypoint.app.easypoint.util.DataConfiguration;

import java.util.List;

public class Item {

	private String            imageName;
	private String            imageID;
	private String            imageTitle;
	private DataConfiguration dataConfiguration;

	public Item(String imageName)
	{
		this.imageName = imageName;
		dataConfiguration = new DataConfiguration();
		List<String> imageNameValues = dataConfiguration.extractValuesFromUri(imageName);
        try {
            imageID = imageNameValues.get(0);
            imageTitle = imageNameValues.get(1);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageID() {
        return imageID;
    }

    public void setImageID(String imageID) {
        this.imageID = imageID;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }
}
