package com.easypoint.app.easypoint.util;


import com.easypoint.app.easypoint.model.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sterlingdiazd on 4/30/2014.
 */
public class DataConfiguration {


    public DataConfiguration() {
    }

    public List<String> extractValuesFromUri(String imageName)
    {
        List<String> imageNameValues = new ArrayList<String>();

        try
        {
            String title = removeFileExtension(imageName);
            String[] imageValues = title.split("-");
            String id = imageValues[0];
            String name = imageValues[1];

            name = beautifyName(name);

            imageNameValues.add(id);
            imageNameValues.add(name);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return imageNameValues;
    }

    public List<Item> configureProductItems(List<String> imageNames)
    {
        List<Item> items = new ArrayList<Item>();

        for (String imageName : imageNames)
        {
            items.add(new Item(imageName));
        }

        return items;
    }

    private String beautifyName(String name)
    {
        String beautifiedName = "";

        if (name.contains("_"))
        {
            String[] words = name.split("_");
            for (int x = 0; x < words.length; x++) {
                String word = words[x];
                beautifiedName += upperCaseFirstLetter(word) + " ";
            }
        } else {
            beautifiedName = upperCaseFirstLetter(name);
        }

        return beautifiedName;
    }

    private String upperCaseFirstLetter(String word)
    {
        String firstLetter = String.valueOf(word.charAt(0));
        String firstUpperCaseLetter = firstLetter.toUpperCase();
        char[] characters = word.toCharArray();
        characters[0] = firstUpperCaseLetter.charAt(0);
        word = String.valueOf(characters);
        return word;
    }

    private String removeFileExtension(String fileName)
    {
        int indexOfPeriod = fileName.indexOf(".");
        return fileName.substring(0, indexOfPeriod);
    }
}