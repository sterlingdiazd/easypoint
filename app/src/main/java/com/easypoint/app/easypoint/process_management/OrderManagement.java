package com.easypoint.app.easypoint.process_management;

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.easypoint.app.easypoint.database.ClientDatabase;
import com.easypoint.app.easypoint.database.OrderDatabase;
import com.easypoint.app.easypoint.database.OrderDetailDatabase;
import com.easypoint.app.easypoint.database.ProductDatabase;
import com.easypoint.app.easypoint.model.Client;
import com.easypoint.app.easypoint.model.Order;
import com.easypoint.app.easypoint.model.OrderDetail;
import com.easypoint.app.easypoint.model.Product;
import com.easypoint.app.easypoint.util.Configuration;
import com.easypoint.app.easypoint.util.Utility;

import java.util.List;

/**
 * Created by sterlingdiazd on 5/28/2014.
 */
public class OrderManagement {

    private FragmentManager fragmentManager;
    private Activity activity;
    private Configuration configuration;
    private ClientDatabase clientDatabase;
    private Client client;
    private String paymentMethod;

    public boolean registerOrder(Activity activity, ToggleButton toggleButtonPaymentMethod, List<Product> cart, Client client)
    {
        boolean successfulOperation = false;
        boolean isOrderProperlyCreated = true;

        if (activity != null && toggleButtonPaymentMethod != null)
        {
            if (cart.isEmpty())
            {
                new AlertDialogManager().showAlertDialog(activity, "Error", "No hay productos", false);
                return successfulOperation;
            }

            if (toggleButtonPaymentMethod.isChecked()) {
                paymentMethod = toggleButtonPaymentMethod.getTextOn() + "";
            } else {
                paymentMethod = toggleButtonPaymentMethod.getTextOff() + "";
            }

            boolean clientIsRequired = false;
            if (paymentMethod.equalsIgnoreCase(toggleButtonPaymentMethod.getTextOn().toString())) {
                clientIsRequired = true;
            }

            OrderDatabase orderDatabase = new OrderDatabase(activity);
            String clientID = null;
            if(clientIsRequired)
            {
                if (client != null)
                {
                    clientID = client.getId();
                }
                else
                {
                    new AlertDialogManager().showAlertDialog(activity, "Error", "No puede crear una venta a credito sin seleccionar un cliente", false);
                    return successfulOperation;
                }
            }
            Order order = new Order(null, clientID, null, new Utility().getDateTime(), paymentMethod, null);
            long idOrder = orderDatabase.addOrder(order);
            String response;

            if (idOrder != -1)
            {
                OrderDetailDatabase orderDetailDatabase = new OrderDetailDatabase(activity);
                ProductDatabase productDatabase = new ProductDatabase(activity);
                for (Product product : cart)
                {
                    OrderDetail orderDetail = new OrderDetail(null, idOrder + "", product.getId(), product.getPrice(), product.getQuantity());
                    orderDetailDatabase.addOrderDetail(orderDetail);
                    product.setStock( ( Integer.valueOf(product.getStock()) - Integer.valueOf(product.getQuantity()) ) + "" );
                    productDatabase.updateProduct(product);
                }

                response = "Order created succesfully";
                successfulOperation = true;
            } else {
                response = "There was a problem creating the order";
            }

            Toast.makeText(activity, response, Toast.LENGTH_SHORT).show();

        }
        return successfulOperation;
    }

}
