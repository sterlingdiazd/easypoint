package com.easypoint.app.easypoint.interfaces;

import com.easypoint.app.easypoint.model.Client;

/**
 * Created by sterlingdiazd on 10/03/2015.
 */
public interface PhaseHandler {

	/**
	 * Request process ins initiated, and the caller is notified throught this method
	 */
	public abstract void onBegan(Object oject );


	public abstract void onError(  Object oject);


	public abstract void onCompleted( Object oject);


	public abstract void onFeedback(Object oject  );

}
