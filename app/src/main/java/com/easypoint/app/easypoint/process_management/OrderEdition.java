package com.easypoint.app.easypoint.process_management;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.database.ClientDatabase;
import com.easypoint.app.easypoint.fragments.FragmentOrder;
import com.easypoint.app.easypoint.model.Client;
import com.easypoint.app.easypoint.model.Product;
import com.easypoint.app.easypoint.util.BitmapManager;
import com.easypoint.app.easypoint.util.Configuration;

import java.util.List;

/**
 * Created by sterlingdiazd on 5/28/2014.
 */
public class OrderEdition {
    private Activity activity;
    private Configuration configuration;
    private ClientDatabase clientDatabase;
    private Client client;

    public OrderEdition(Activity activity) {
        this.activity = activity;
        configuration = Configuration.getInstance(activity);
        configuration.configureSharePreference();
    }

    public boolean setDialogOrderEdition(final Product product, final List<Product> cart, boolean isDialogShown, final FragmentOrder fragmentOrder) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setTitle("Editar Compra");
        //alertDialogBuilder.setIcon( );

        LayoutInflater inflater = LayoutInflater.from(activity);
        View rootView = inflater.inflate(R.layout.order_item_edition, null);

        final EditText editTextEditProductQuantity;
        final EditText editTextEditProductName;
        final EditText editTextEditProductPrice;

        editTextEditProductQuantity = (EditText) rootView.findViewById(R.id.editTextEditProductQuantity);
        editTextEditProductName = (EditText) rootView.findViewById(R.id.editTextEditProductName);
        editTextEditProductPrice = (EditText) rootView.findViewById(R.id.editTextEditProductPrice);
        ImageView imageViewEditProduct = (ImageView) rootView.findViewById(R.id.imageViewEditProduct);

        String imagePath = activity.getFilesDir() + "/" + Configuration.IMAGE_DIRECTORY + "/" + product.getImagePath();

        imageViewEditProduct.setImageBitmap(new BitmapManager().decodeSampledBitmapFromResource(imagePath, 120, 150));
        editTextEditProductQuantity.setText(product.getQuantity());
        editTextEditProductName.setText(product.getDescription());
        editTextEditProductPrice.setText(product.getPrice());
        editTextEditProductName.setEnabled(false);
        editTextEditProductPrice.setEnabled(false);

        alertDialogBuilder.setView(rootView);

        alertDialogBuilder.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String quantity = editTextEditProductQuantity.getText().toString();
                String name = editTextEditProductName.getText().toString();
                String price = editTextEditProductPrice.getText().toString();

                if (quantity.length() > 0 && name.length() > 0 && price.length() > 0) {
                    product.setQuantity(quantity);
                    product.setDescription(name);
                    product.setPrice(price);

                    for (Product prod : cart) {
                        if (prod.getId().equalsIgnoreCase(product.getId())) {
                            prod.setQuantity(quantity);
                            prod.setDescription(name);
                            prod.setPrice(price);
                            break;
                        }
                    }

                    fragmentOrder.updateCart(cart, fragmentOrder.getAdapterListViewCart(), fragmentOrder.getListViewCart(), fragmentOrder.getTextViewOrderTotal(),
                            fragmentOrder.getClient());

                    dialog.cancel();

                } else {
                    AlertDialogManager alert = new AlertDialogManager();
                    alert.showAlertDialog(activity, "Campos requeridos", "Debe llenar todos los campos", false);
                }


            }
        });

        alertDialogBuilder.create();
        alertDialogBuilder.show();
        isDialogShown = true;
        return isDialogShown;
    }

}
