package com.easypoint.app.easypoint.model;

/**
 * Created by sterlingdiazd on 5/23/2014.
 */
public class OrderDetail {

	private String idOrderDetail;
	private String idOrder;
	private String idProduct;
	private String unitPrice;
	private String quantity;

	public OrderDetail(String idOrderDetail, String idOrder, String idProduct, String unitPrice, String quantity)
	{
		this.idOrderDetail = idOrderDetail;
        this.idOrder = idOrder;
        this.idProduct = idProduct;

        this.unitPrice = unitPrice;
        this.quantity = quantity;
    }

    public String getIdOrderDetail() {
        return idOrderDetail;
    }

    public void setIdOrderDetail(String idOrderDetail) {
        this.idOrderDetail = idOrderDetail;
    }

    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
