package com.easypoint.app.easypoint.process_management;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.adapters.AdapterListViewClient;
import com.easypoint.app.easypoint.database.ClientDatabase;
import com.easypoint.app.easypoint.fragments.FragmentOrder;
import com.easypoint.app.easypoint.model.Client;
import com.easypoint.app.easypoint.util.Configuration;
import com.easypoint.app.easypoint.util.Utility;

/**
 * Created by sterlingdiazd on 5/28/2014.
 */
public class ClientManager
{

	private FragmentManager fragmentManager;
	private Activity        activity;
	private Configuration   configuration;
	private ClientDatabase  clientDatabase;

	public ClientManager(Activity activity)
	{
		this.activity = activity;
		configuration = Configuration.getInstance(activity);
		configuration.configureSharePreference();
		clientDatabase = new ClientDatabase(activity);
	}

	public void setRegisterClientDialog(final Context context)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Registrar Cliente");
		//alertDialogBuilder.setIcon( );

		LayoutInflater inflater = LayoutInflater.from(context);
		View rootView = inflater.inflate(R.layout.dialog_register_client, null);

		final EditText editTextCedula;
		final EditText editTextName;
		final EditText editTextLastName;
		final EditText editTextEmail;
		final EditText editTextDireccion;
		final Button buttonFechaNac;
		final EditText editTextBirthdate;
		final EditText editTextCellphone;

		final String[] cedula = new String[1];
        final String[] name = new String[1];
        final String[] lastname = new String[1];
        final String[] email = new String[1];
        final String[] direccion = new String[1];
        final String[] birthdate = new String[1];
        final String[] cellphone = new String[1];

        editTextCedula = (EditText) rootView.findViewById(R.id.cedula);
        editTextName = (EditText) rootView.findViewById(R.id.editTextName);
        editTextLastName = (EditText) rootView.findViewById(R.id.editTextLastName);
        editTextEmail = (EditText) rootView.findViewById(R.id.editTextEmail);
        editTextDireccion = (EditText) rootView.findViewById(R.id.editTextDireccion);
        buttonFechaNac = (Button) rootView.findViewById(R.id.buttonFechaNac);
        editTextBirthdate = (EditText) rootView.findViewById(R.id.editTextBirthdate);
        editTextCellphone = (EditText) rootView.findViewById(R.id.editTextCellphone);


        final FragmentManager fragmentManager = activity.getFragmentManager();
        final FragmentOrder fragmentOrder = (FragmentOrder) fragmentManager.findFragmentById(R.id.main_frame);

        editTextBirthdate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final DatePickerFragment datePickerFragment = new DatePickerFragment();

                datePickerFragment.show(fragmentManager, "datepicker");
                datePickerFragment.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String date = new Utility().createDateWithHyphen(year, monthOfYear + 1, dayOfMonth);
                        editTextBirthdate.setText(date);
                    }
                });
            }
        });

        buttonFechaNac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final DatePickerFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.show(fragmentManager, "datepicker");
                datePickerFragment.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String date = new Utility().createDateWithHyphen(year, monthOfYear + 1, dayOfMonth);
                        editTextBirthdate.setText(date);
                    }
                });

                buttonFechaNac.setVisibility(View.GONE);
                editTextBirthdate.setVisibility(View.VISIBLE);
            }
        });



        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                cedula[0] = editTextCedula.getText().toString();
                name[0] = editTextName.getText().toString();
                lastname[0] = editTextLastName.getText().toString();
                email[0] = editTextEmail.getText().toString();
                direccion[0] = editTextDireccion.getText().toString();
                birthdate[0] = editTextBirthdate.getText().toString();
                cellphone[0] = editTextCellphone.getText().toString();

                Client client = null;
                boolean clientInserted = false;
                if(cedula[0].length() > 0  || name[0].length() > 0 || lastname[0].length() > 0 || email[0].length()  > 0  || direccion[0].length()  > 0 || cellphone[0].length()  > 0
                        || birthdate[0].length()  > 0 || cellphone[0].length()  > 0 )
                {

                    String geolocalizacion = configuration.getPrefs().getString("geolocalizacion", ""); //Get Geolocalizacion creada en ventana anterior
                    client = new Client(null, cedula[0], name[0], lastname[0], email[0], direccion[0], null, null, birthdate[0], cellphone[0]);
                    clientInserted = clientDatabase.addClient(client);
                    dialog.cancel();

                }
                else
                {
                    AlertDialogManager alert = new AlertDialogManager();
                    alert.showAlertDialog(activity, "Campos requeridos", "Debe llenar todos los campos", false);
                }

                if(clientInserted== true)
                {
                    //clientDatabase.getClientByPersonalID(cedula[0]);
                    fragmentOrder.setClient(client);
                }


                TextView textViewClientName = (TextView) fragmentOrder.getView().findViewById(R.id.textViewClientName);
                textViewClientName.setText(client.getName());

            }
        });
		alertDialogBuilder.setView(rootView);
        alertDialogBuilder.create();
        alertDialogBuilder.show();
    }

    public void setEditClientDialog(final Context context, final Client client, final ListView listViewClientList, final AdapterListViewClient adapterListViewClient)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( context );
        alertDialogBuilder.setTitle(  "Editar Cliente" );
        //alertDialogBuilder.setIcon( );

        LayoutInflater inflater = LayoutInflater.from(context);
        View rootView = inflater.inflate(R.layout.dialog_register_client, null);

        final EditText editTextCedula;
        final EditText editTextName;
        final EditText editTextLastName;
        final EditText editTextEmail;
        final EditText editTextDireccion;
        final Button buttonFechaNac;
        final EditText editTextBirthdate;
        final EditText editTextCellphone;

        editTextCedula = (EditText) rootView.findViewById(R.id.cedula);
        editTextName = (EditText) rootView.findViewById(R.id.editTextName);
        editTextLastName = (EditText) rootView.findViewById(R.id.editTextLastName);
        editTextEmail = (EditText) rootView.findViewById(R.id.editTextEmail);
        editTextDireccion = (EditText) rootView.findViewById(R.id.editTextDireccion);
        buttonFechaNac = (Button) rootView.findViewById(R.id.buttonFechaNac);
        editTextBirthdate = (EditText) rootView.findViewById(R.id.editTextBirthdate);
        editTextCellphone = (EditText) rootView.findViewById(R.id.editTextCellphone);

        buttonFechaNac.setVisibility(View.GONE);
        editTextBirthdate.setVisibility(View.VISIBLE);

        editTextCedula.setText(client.getCedula());
        editTextName.setText(client.getName());
        editTextLastName.setText(client.getLastname());
        editTextEmail.setText(client.getEmail());
        editTextDireccion.setText(client.getDireccion());
        editTextBirthdate.setText(client.getBirthdate());
        editTextCellphone.setText(client.getCellphone());

//        final FragmentManager fragmentManager = ((EasyPointActivity) activity).getSupportFragmentManager();
//        final EasyPointActivity.PlaceholderFragment placeholderFragment = (EasyPointActivity.PlaceholderFragment) fragmentManager.findFragmentById(R.id.container);
//
//        editTextBirthdate.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                final DatePickerFragment datePickerFragment = new DatePickerFragment();
//
//                datePickerFragment.show(fragmentManager, "datepicker");
//                datePickerFragment.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        String date = new Utility().createDateWithHyphen(year, monthOfYear + 1, dayOfMonth);
//                        editTextBirthdate.setText(date);
//                    }
//                });
//            }
//        });

        buttonFechaNac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final DatePickerFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.show(fragmentManager, "datepicker");
                datePickerFragment.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String date = new Utility().createDateWithHyphen(year, monthOfYear + 1, dayOfMonth);
                        editTextBirthdate.setText(date);
                    }
                });

                buttonFechaNac.setVisibility(View.GONE);
                editTextBirthdate.setVisibility(View.VISIBLE);
            }
        });

        alertDialogBuilder.setView(rootView);

//        alertDialogBuilder.setPositiveButton("Guardar Edicion", new DialogInterface.OnClickListener()
//        {
//            public void onClick(DialogInterface dialog, int which)
//            {
//                client.setCedula(editTextCedula.getText().toString());
//                client.setName(editTextName.getText().toString());
//                client.setLastname(editTextLastName.getText().toString());
//                client.setEmail(editTextEmail.getText().toString());
//                client.setDireccion(editTextDireccion.getText().toString());
//                client.setBirthdate(editTextBirthdate.getText().toString());
//                client.setCellphone(editTextCellphone.getText().toString());
//
//                //String geolocalizacion = configuration.getPrefs().getString("geolocalizacion", ""); //Get Geolocalizacion creada en ventana anterior
//                clientDatabase = new ClientDatabase(activity);
//                clientDatabase.updateClient(new Client(client.getId(), client.getCedula(), client.getName(), client.getLastname(), client.getEmail(), client.getDireccion(),
//                        null, null,
//                        client.getBirthdate(), client.getCellphone()));
//                adapterListViewClient.setClients(new ClientDatabase(activity).getAllClients());
//                listViewClientList.setAdapter(adapterListViewClient);
//                listViewClientList.refreshDrawableState();
//                dialog.cancel();
//            }
//        });

        alertDialogBuilder.create();
        alertDialogBuilder.show();
    }
}
