package com.easypoint.app.easypoint.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.model.Client;
import com.easypoint.app.easypoint.util.Configuration;

import java.util.ArrayList;
import java.util.List;


public class AdapterListViewClient extends BaseAdapter
{

	private Activity     activity;
	private List<Client> clients;

	public List<Client> getClients()
	{
		return clients;
	}

	public void setClients(List<Client> clients)
	{
		this.clients = clients;
	}

	public AdapterListViewClient(Activity activity)
	{
		this.activity = activity;
		this.clients = new ArrayList<Client>();
		Configuration.getInstance(activity).configureSharePreference();
	}

	public View getView(int position, View view, ViewGroup viewGroup)
	{
		LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View product_item = layoutInflater.inflate(R.layout.adapter_client, null);

        TextView textViewAdapterCientID = (TextView) product_item.findViewById(R.id.textViewAdapterCientID);
        TextView textViewAdapterClientName = (TextView) product_item.findViewById(R.id.textViewAdapterClientName);

        Client client = clients.get(position);

        textViewAdapterCientID.setText( client.getId() );
        textViewAdapterClientName.setText( client.getName() + " " + client.getLastname() );

        return product_item;
    }

    public int getCount() {
        return clients.size();
    }

    public Object getItem(int position) {
        return clients.get(position);
    }

    public long getItemId(int id) {
        return id;
    }



}
