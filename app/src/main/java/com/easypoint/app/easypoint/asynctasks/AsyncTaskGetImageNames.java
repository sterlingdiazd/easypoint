package com.easypoint.app.easypoint.asynctasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.util.Configuration;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by sterlingdiazd on 23/01/2015.
 */
public class AsyncTaskGetImageNames extends AsyncTask<String, Integer, Boolean>
{

	public  ProgressDialog progressDialog;
	private Configuration  configuration;
	private String         responseMessage;
	private Activity       activity;
	private JSONArray      data;
	private boolean        isSync;

	public AsyncTaskGetImageNames(Activity activity, boolean isSync)
	{
		this.activity = activity;
		this.isSync = isSync;
		configuration = Configuration.getInstance(activity);
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();
		progressDialog = new ProgressDialog(activity);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage(activity.getResources().getString(R.string.configuring_image_download));
		progressDialog.show();
	}

	public Boolean doInBackground(String... params)
	{

		boolean operationWasSuccessfull = false;

		try
		{
			ArrayList<NameValuePair> parametros = new ArrayList<NameValuePair>();
			parametros.add(new BasicNameValuePair(Configuration.ENTIDAD, params[0]));

			HttpPost post = new HttpPost(Configuration.HOST + "/Controllers/entry_point.php");
			try
			{
				post.setEntity(new UrlEncodedFormEntity(parametros));
			}
			catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}

			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse resp = null;
			try
			{
				resp = httpClient.execute(post);
				int status = resp.getStatusLine().getStatusCode();

				if (status == 200)
				{
					HttpEntity httpEntity = resp.getEntity();
					InputStream is = null;
					try
					{
						is = httpEntity.getContent();
						if (is != null)
						{
							BufferedReader rd = null;
							try
							{
								rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
							}
							catch (UnsupportedEncodingException e)
							{
								e.printStackTrace();
							}
							StringBuilder sb = new StringBuilder();
							String line = "";
							try
							{
								while ((line = rd.readLine()) != null)
								{
									sb.append(line + "\n");
								}
								is.close();

								String result = sb.toString();
								//String json = result.substring(result.indexOf('[') + 1, result.lastIndexOf(']'));
								JSONObject o = new JSONObject(result);
								responseMessage = o.getString(Configuration.MESSAGE);

								if (o.getString(Configuration.RESULT).equalsIgnoreCase(Configuration.SUCCESS))
								{
									data = new JSONArray(o.getString(Configuration.DATA));

									operationWasSuccessfull = true;
								}

							}
							catch (IOException e)
							{
								e.printStackTrace();
							}
						}
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					return null;
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}



		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			Log.e("ServicioRest", "Error: " + ex.getMessage() + "");
		}

		return operationWasSuccessfull;
	}

	protected void onPostExecute(Boolean result)
	{

		progressDialog.dismiss();
		Toast.makeText(activity, responseMessage, Toast.LENGTH_LONG).show();
		Configuration.imageNames = convertJSONArrayToArrayList(data);

		if (result && isSync == false)
		{
			new AsyncTaskDownloaImage(activity).execute();
		}
		else
		{
			// Se carga sin internet, y si se necesitan agregar mas, se descargan, y se actualiza el gridview agregando las nuevas.
		}
	}

	public ArrayList<String> convertJSONArrayToArrayList(JSONArray jsonArray)
	{
		ArrayList<String> imageNames = new ArrayList<String>();

		for (int x = 0; x < jsonArray.length(); x++)
		{
			try
			{
				String imageName = jsonArray.getJSONObject(x).getString(Configuration.IMAGE_PATH);
				imageNames.add(imageName);
			}
			catch (JSONException jsonException)
			{
				jsonException.printStackTrace();
			}
		}
		return imageNames;
	}

}