package com.easypoint.app.easypoint.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.easypoint.app.easypoint.database.OrderDetailDatabase;
import com.easypoint.app.easypoint.interfaces.PhaseHandler;
import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.adapters.AdapterListViewClientOrders;
import com.easypoint.app.easypoint.database.ClientDatabase;
import com.easypoint.app.easypoint.model.Client;
import com.easypoint.app.easypoint.model.OrderDetail;
import com.easypoint.app.easypoint.process_management.ClientSearch;
import com.easypoint.app.easypoint.util.Configuration;
import com.easypoint.app.easypoint.util.DataConfiguration;
import com.easypoint.app.easypoint.util.EntitySearches;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentOrder.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentOrder#newInstance} factory method to
 * create an instance of this fragment.
 */

public class FragmentSales extends Fragment
{
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_PARAM1 = "param1";
	private static final String ARG_PARAM2 = "param2";

	// TODO: Rename and change types of parameters
	private       String                        mParam1;
	private       String                        mParam2;
	private       OnFragmentInteractionListener mListener;
	public static TabHost                       tabs;
	private       TabHost.TabSpec               spec;
	private       Configuration                 configuration;
	private       DataConfiguration             dataConfiguration;

	private TextView                    textViewClientOrderTitle;
	private Client                      client;
	private Activity                    activity;
	private AdapterListViewClientOrders adapterListViewClientOrders;
	private ListView                    listViewClientOrders;
	private List<OrderDetail>           orderDetails;

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param param1 Parameter 1.
	 * @param param2 Parameter 2.
	 * @return A new instance of fragment ProductSelection.
	 */
	// TODO: Rename and change types and number of parameters
	public static FragmentSales newInstance(String param1, String param2)
	{
		FragmentSales fragment = new FragmentSales();
		Bundle args = new Bundle();
		args.putString(ARG_PARAM1, param1);
		args.putString(ARG_PARAM2, param2);
		fragment.setArguments(args);
		return fragment;
	}

	public FragmentSales()
	{
		// Required empty public constructor
	}

	@Override
	public void onAttach(final Activity activity)
	{
		super.onAttach(activity);
		try
		{

			mListener = (OnFragmentInteractionListener) activity;
			this.activity = activity;


			//new ClientSearch().clientListDialog(activity, handler);

			//adapterListViewClientOrders.notifyDataSetChanged();
			/*
			Bundle bundle = getArguments();
			if (bundle != null)
			{
				products = (List<Product>) bundle.getSerializable("products");
			}
			else
			{
				productDatabase = new ProductDatabase(activity);
				products = productDatabase.getAllProducts();
			}

			 */


		}
		catch (ClassCastException e)
		{
			throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
		}
	}



	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		menu.clear();
		inflater.inflate(R.menu.sales, menu);

		SearchView search = (SearchView) menu.findItem(R.id.action_search).getActionView();
		search.setQueryHint(getString(R.string.search_sales));
		search.setOnQueryTextListener(queryListener);

		int id = search.getContext().getResources()
				.getIdentifier("android:id/search_src_text", null, null);

		TextView text = (TextView) search.findViewById(id);
		text.setHintTextColor(Color.WHITE);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		switch (id) {
			/*
		case R.id.action_search:
				Log.e("e", "search");
				return true;
			case R.id.action_add:

				return true;
			case R.id.action_edit:
				Log.e("e", "action add sell");
				return true;
			case R.id.action_validate:
				Log.e("e", "action add sell");
				return true;
				*/
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	private final SearchView.OnQueryTextListener queryListener = new SearchView.OnQueryTextListener()
	{
		@Override
		public boolean onQueryTextSubmit(String query)
		{
			return true;
		}
		
		@Override
		public boolean onQueryTextChange(String query)
		{
			if (query != null)
			{
				/*
				EntitySearches entitySearch = new EntitySearches();
				List<Client> clients = new ArrayList<Client>();
				ClientDatabase clientDatabase = new ClientDatabase(activity);
				
				if (client == null)
				{
					clients = (List<Client>) entitySearch.searchEntities(query, clients, clientDatabase);
					if (clients.size() > 1)
					{
						new ClientSearch().clientListDialog(activity, null).show();
					}
					else if (clients.size() == 1)
					{
						client = clients.get(0);
						orderDetails = new EntitySearches().retrieveOrderDataForClient(activity, client);
					}
					else
					{
						orderDetails = (List<OrderDetail>) new OrderDetailDatabase(activity).getAll();
					}
					
				}
				else
				{
					orderDetails = new EntitySearches().retrieveOrderDataForClient(activity, client);
				}
				String title;
				if (client != null)
				{
					title = textViewClientOrderTitle.getText().toString() + " " + client.getName() + " " + client.getLastname();
				}
				else
				{
					title = textViewClientOrderTitle.getText().toString();
				}
				
				textViewClientOrderTitle.setText(title);
				adapterListViewClientOrders.setOrderDetails(orderDetails);
				listViewClientOrders.setAdapter(adapterListViewClientOrders);
				listViewClientOrders.refreshDrawableState();
				*/
				return true;
			}
			else
			{
				return false;
			}
		}
	};
	
	private final PhaseHandler handler = new PhaseHandler()
	{
		@Override
		public void onBegan(Object oject)
		{
		}
		
		@Override
		public void onError(Object oject)
		{
		}
		
		@Override
		public void onCompleted(Object object)
		{
			/*
			client = (Client) object;
			initComponent(getView());
		*/
			/*
			adapterListViewClientOrders.setClient(activity, client);
			adapterListViewClientOrders.notifyDataSetChanged();
			*/
		}
		
		@Override
		public void onFeedback(Object oject)
		{
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		if (getArguments() != null)
		{
			mParam1 = getArguments().getString(ARG_PARAM1);
			mParam2 = getArguments().getString(ARG_PARAM2);
		}
		
		configuration = Configuration.getInstance(activity);
		dataConfiguration = new DataConfiguration();
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_sales, container, false);
		initComponent(view);
		setHasOptionsMenu(true);
		return view;
	}
	
	
	@Override
	public void onPause()
	{
		super.onPause();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		
	}
	
	private void initComponent(View view)
	{
		try
		{
			textViewClientOrderTitle = (TextView) view.findViewById(R.id.textViewSalesTitle);
			listViewClientOrders = (ListView) view.findViewById(R.id.listViewClientOrders);
			orderDetails = (List<OrderDetail>) new OrderDetailDatabase(activity).getAll();
			adapterListViewClientOrders = new AdapterListViewClientOrders(activity, orderDetails);
			listViewClientOrders.setAdapter(adapterListViewClientOrders);
			/*
			if (client != null)
			{
				orderDetails = new EntitySearches().retrieveOrderDataForClient(activity, client);

				listViewClientOrders.setAdapter(adapterListViewClientOrders);

				//alertDialogBuilder.setTitle( );

			}
			else
			{
				orderDetails = (List<OrderDetail>) new OrderDetailDatabase(activity).getAll();
				adapterListViewClientOrders = new AdapterListViewClientOrders(activity, orderDetails);
				listViewClientOrders.setAdapter(adapterListViewClientOrders);
				Log.e("e", "client is null");
			}
			*/
			
		}
		catch (Exception e)
		{
			Toast.makeText(activity, e.getMessage(), Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void onDetach()
	{
		super.onDetach();
		mListener = null;
	}
	
	
	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener
	{
		// TODO: Update argument type and name
		void onClientSelected(Client client);
	}
}
