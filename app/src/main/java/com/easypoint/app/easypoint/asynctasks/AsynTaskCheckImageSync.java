package com.easypoint.app.easypoint.asynctasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.widget.Toast;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.util.Configuration;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by sterlingdiazd on 3/1/15.
 */
public class AsynTaskCheckImageSync extends AsyncTask<String, Integer, Boolean>
{

	public  ProgressDialog progressDialog;
	private String         responseMessage;
	private Activity       activity;
	private String         data;
	private Configuration  configuration;

	public AsynTaskCheckImageSync(Activity activity)
	{
		this.activity = activity;
		configuration = Configuration.getInstance(activity);
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();
		progressDialog = new ProgressDialog(activity);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage(activity.getResources().getString(R.string.checking_product_image_sync));
		progressDialog.show();
	}

	public Boolean doInBackground(String... params)
	{

		boolean operationWasSuccessfull = false;

		try
		{
			ArrayList<NameValuePair> parametros = new ArrayList<NameValuePair>();
			parametros.add(new BasicNameValuePair(Configuration.ENTIDAD, params[0]));
			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());

			HttpPost post = new HttpPost(Configuration.HOST + "/Controllers/entry_point.php");
			try
			{
				post.setEntity(new UrlEncodedFormEntity(parametros));
			} catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}

			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse resp = null;
			try
			{
				resp = httpClient.execute(post);
				int status = resp.getStatusLine().getStatusCode();

				if (status == 200)
				{
					HttpEntity httpEntity = resp.getEntity();
					InputStream is = null;
					try
					{
						is = httpEntity.getContent();
						if (is != null)
						{
							BufferedReader rd = null;
							try
							{
								rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
							} catch (UnsupportedEncodingException e)
							{
								e.printStackTrace();
							}
							StringBuilder sb = new StringBuilder();
							String line = "";
							try
							{
								while ((line = rd.readLine()) != null)
								{
									sb.append(line + "\n");
								}
								is.close();

								String result = sb.toString();
								//String json = result.substring(result.indexOf('[') + 1, result.lastIndexOf(']'));
								JSONObject o = new JSONObject(result);
								responseMessage = o.getString(Configuration.MESSAGE);

								if (o.getString(Configuration.RESULT).equalsIgnoreCase(Configuration.SUCCESS))
								{
									data = o.getString(Configuration.DATA);
									operationWasSuccessfull = true;
								}

							} catch (IOException e)
							{
								e.printStackTrace();
							}
						}
					} catch (IOException e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					return null;
				}
			} catch (IOException e)
			{
				e.printStackTrace();
			}

		} catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return operationWasSuccessfull;
	}

	protected void onPostExecute(Boolean result)
	{
		progressDialog.dismiss();

		if (result)
		{
			try
			{
				int savedQuantity = configuration.getPrefs().getInt(Configuration.PRODUCT_IMAGES_QUANTITY, 0);
				int receivedQuantity = Integer.valueOf(data);
				boolean isSync = false;

				if (savedQuantity != receivedQuantity && receivedQuantity != 0)
				{
					if (savedQuantity < receivedQuantity)
					{
						Toast.makeText(activity, activity.getResources().getString(R.string.new_products_available), Toast.LENGTH_LONG).show();

						new AsyncTaskGetImageNames(activity, isSync).execute(Configuration.GET_IMAGES_NAMES);
						//Mostrar alerta para que el usuario le de a Sincronizar productos y se descarguen los que le faltan.

					} else
					{

					}
				} else
				{
					Toast.makeText(activity, activity.getResources().getString(R.string.products_syncronized), Toast.LENGTH_LONG).show();
					isSync = true;
					if(receivedQuantity != 0)
					{
						Toast.makeText(activity, "Productos Actualizados", Toast.LENGTH_LONG).show();
					}
					else
					{
						Toast.makeText(activity, "No hay productos en su cuenta", Toast.LENGTH_LONG).show();
					}


				}
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}

	}
}
