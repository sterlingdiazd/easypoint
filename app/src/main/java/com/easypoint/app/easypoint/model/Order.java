package com.easypoint.app.easypoint.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sterlingdiazd on 5/23/2014.
 */
public class Order implements Serializable
{

	private String            idOrder;
	private String            idClient;
	private String            idEmployee;
	private String            orderDateTime;
	private String            paymentMethod;
	private List<OrderDetail> orderDetails;


	public Order(String idOrder, String idClient, String idEmployee, String orderDateTime, String paymentMethod, List<OrderDetail> orderDetails)
	{
		this.idOrder = idOrder;
		this.idClient = idClient;
        this.idEmployee = idEmployee;
        this.orderDateTime = orderDateTime;
        this.paymentMethod = paymentMethod;
        this.orderDetails = orderDetails;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(String idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getOrderDateTime() {
        return orderDateTime;
    }

    public void setOrderDateTime(String orderDateTime) {
        this.orderDateTime = orderDateTime;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }
}
