package com.easypoint.app.easypoint.database;

import java.util.List;

/**
 * Created by sterlingdiazd on 03/05/2015.
 */
public interface SearchableDatabase
{
	Object getByName(String name);
	Object getAll();
}
