package com.easypoint.app.easypoint.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.model.Item;
import com.easypoint.app.easypoint.model.Product;
import com.easypoint.app.easypoint.util.BitmapManager;
import com.easypoint.app.easypoint.util.Configuration;

import java.util.List;


public class AdapterGridView extends BaseAdapter
{

	private Activity      activity;
	private List<Item>    items;
	private List<Product> products;

	public List<Product> getProducts()
	{
		return products;
	}

	public void setProducts(List<Product> products)
	{
		this.products = products;
	}

	public AdapterGridView(Activity activity, List<Item> items, List<Product> products)
	{
		this.activity = activity;
		this.items = items;

		this.products = products;
		Configuration.getInstance(activity);
	}

	public View getView(int position, View view, ViewGroup viewGroup)
	{
		LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View gridviewItem = layoutInflater.inflate(R.layout.gridview_item, null);

		Item item = items.get(position);

		ImageView imageView = (ImageView) gridviewItem.findViewById(R.id.imageViewIcon);
		TextView textViewIconTitle = (TextView) gridviewItem.findViewById(R.id.textViewIconTitle);
		textViewIconTitle.setText(item.getImageTitle());

		String imagePath = activity.getFilesDir() + "/" + Configuration.IMAGE_DIRECTORY + "/" + item.getImageName();
		Product product = products.get(position);
		//product.setImagePath(imagePath);

		Bitmap bitmap = new BitmapManager().decodeSampledBitmapFromResource(imagePath, 120, 160);
		if (bitmap != null)
		{
			imageView.setImageBitmap(bitmap);
		} else {
            imageView.setImageResource(R.drawable.ic_launcher);
        }

        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        gridviewItem.setLayoutParams(new GridView.LayoutParams(120, 160));
        //imageView.setPadding(10, 10, 10, 10);

		return gridviewItem;
	}

    public int getCount() {
        return items.size();
    }

    public Object getItem(int position) {
        return items.get(position);
    }

    public long getItemId(int id) {
        return id;
    }

}
