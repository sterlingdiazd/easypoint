package com.easypoint.app.easypoint.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.model.Item;
import com.easypoint.app.easypoint.model.Product;
import com.easypoint.app.easypoint.util.BitmapManager;
import com.easypoint.app.easypoint.util.Configuration;

import java.util.List;


public class AdapterListViewFragmentProducts extends BaseAdapter
{

	private Activity      activity;
	private List<Product> products;

	public List<Product> getProducts()
	{
		return products;
	}

	public void setProducts(List<Product> products)
	{
		this.products = products;
	}

	public AdapterListViewFragmentProducts(Activity activity, List<Product> products)
	{
		this.activity = activity;
		this.products = products;
	}

	public View getView(int position, View view, ViewGroup viewGroup)
	{
		LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View listviewItem = layoutInflater.inflate(R.layout.layout_product, null);

		Product product = products.get(position);

		ImageView imageViewFragmentProducImage = (ImageView) listviewItem.findViewById(R.id.imageViewFragmentProducImage);
		TextView textViewProductCategory = (TextView) listviewItem.findViewById(R.id.textViewProductCategory);
		TextView textViewProductDescription = (TextView) listviewItem.findViewById(R.id.textViewProductDescription);
		TextView textViewProductSupplier = (TextView) listviewItem.findViewById(R.id.textViewProductSupplier);
		TextView textViewProductCost = (TextView) listviewItem.findViewById(R.id.textViewProductCost);
		TextView textViewProductPrice = (TextView) listviewItem.findViewById(R.id.textViewProductPrice);
		TextView textViewProductExpireDate = (TextView) listviewItem.findViewById(R.id.textViewProductExpireDate);
		TextView textViewProductStock = (TextView) listviewItem.findViewById(R.id.textViewProductStock);
		TextView textViewProductStockLimit = (TextView) listviewItem.findViewById(R.id.textViewProductStockLimit);

		textViewProductCategory.setText(product.getCategory());
		textViewProductDescription.setText(product.getDescription());
		textViewProductSupplier.setText(product.getSupplierId());
		textViewProductCost.setText(product.getCost());
		textViewProductPrice.setText(product.getPrice());
		textViewProductExpireDate.setText(product.getExpireDate());
		textViewProductStock.setText(product.getStock());
		textViewProductStockLimit.setText(product.getLowLimitStock());

		String imagePath = activity.getFilesDir() + "/" + Configuration.IMAGE_DIRECTORY + "/" + product.getImagePath();

		Bitmap bitmap = new BitmapManager().decodeSampledBitmapFromResource(imagePath, 36, 48);
		product.setBitmap(bitmap);
		if (bitmap != null)
		{
			imageViewFragmentProducImage.setImageBitmap(bitmap);
		} else {
			imageViewFragmentProducImage.setImageResource(R.drawable.ic_launcher);
        }
		imageViewFragmentProducImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        //listviewItem.setLayoutParams(new GridView.LayoutParams(120, 160));
        //imageView.setPadding(10, 10, 10, 10);

		return listviewItem;
	}

    public int getCount() {
        return products.size();
    }

    public Object getItem(int position) {
        return products.get(position);
    }

    public long getItemId(int id) {
        return id;
    }

}
