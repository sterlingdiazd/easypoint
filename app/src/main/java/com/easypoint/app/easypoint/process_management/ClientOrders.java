package com.easypoint.app.easypoint.process_management;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.adapters.AdapterListViewClientOrders;
import com.easypoint.app.easypoint.database.ClientDatabase;
import com.easypoint.app.easypoint.model.Client;
import com.easypoint.app.easypoint.model.OrderDetail;
import com.easypoint.app.easypoint.util.EntitySearches;

import java.util.List;


/**
 * Created by sterlingdiazd on 6/1/2014.
 */
public class ClientOrders
{
    private TextView                    textViewClientOrderTitle;
    private Client                      client;
    private Activity                    activity;
    private ClientDatabase              clientDatabase;
    private AdapterListViewClientOrders adapterListViewClientOrders;
    private ListView                    listViewClientOrders;
    private List<OrderDetail>           orderDetails;

    public Dialog clientOrdersDialog(final Activity activity, Client client)
    {
        this.activity = activity;
        clientDatabase = new ClientDatabase(activity);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = LayoutInflater.from(activity);
        View rootView = inflater.inflate(R.layout.fragment_sales, null);

        textViewClientOrderTitle = (TextView) rootView.findViewById(R.id.textViewSalesTitle);
        listViewClientOrders = (ListView) rootView.findViewById(R.id.listViewClientOrders);

        orderDetails = new EntitySearches().retrieveOrderDataForClient(activity, client);
        adapterListViewClientOrders = new AdapterListViewClientOrders(activity, orderDetails);
        listViewClientOrders.setAdapter(adapterListViewClientOrders);

        //alertDialogBuilder.setTitle( );
        textViewClientOrderTitle.setText(textViewClientOrderTitle.getText().toString() + " " + client.getName() + " " + client.getLastname());

        //alertDialogBuilder.setIcon( );
        alertDialogBuilder.setView(rootView);

        AlertDialog dialog = alertDialogBuilder.create();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (int) (WindowManager.LayoutParams.MATCH_PARENT);
        lp.height = (int) (WindowManager.LayoutParams.MATCH_PARENT);

        dialog.show();
        dialog.getWindow().setAttributes(lp);

        listViewClientOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
               // dialog.dismiss();
            }
        });


        return dialog;
    }

}
