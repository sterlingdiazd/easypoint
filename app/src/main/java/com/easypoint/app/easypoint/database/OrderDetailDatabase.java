package com.easypoint.app.easypoint.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.easypoint.app.easypoint.model.OrderDetail;

import java.util.ArrayList;
import java.util.List;


public class OrderDetailDatabase extends SQLiteOpenHelper  implements SearchableDatabase
{

	public static final  String TABLE_NAME_ORDER_DETAIL                  = "OrderDetail";
	public static final  String COLUMN_NAME_ORDER_DETAIL_ID_ORDER_DETAIL = "idOrderDetail";
	public static final  String COLUMN_NAME_ORDER_DETAIL_ID_ORDER        = "idOrder";
	public static final  String COLUMN_NAME_ORDER_DETAIL_ID_PRODUCT      = "idProduct";
	public static final  String COLUMN_NAME_ORDER_DETAIL_UNIT_PRICE      = "unitPrice";
	public static final  String COLUMN_NAME_ORDER_DETAIL_QUANTITY        = "quantity";
	private static final int    DATABASE_VERSION                         = 1;

	private String createTable_order_detail = "create table " +
	                                          TABLE_NAME_ORDER_DETAIL + " ( " +
	                                          COLUMN_NAME_ORDER_DETAIL_ID_ORDER_DETAIL + " integer primary key autoincrement , " +
	                                          COLUMN_NAME_ORDER_DETAIL_ID_ORDER + " TEXT , " +
	                                          COLUMN_NAME_ORDER_DETAIL_ID_PRODUCT + " TEXT , " +
	                                          COLUMN_NAME_ORDER_DETAIL_UNIT_PRICE + " TEXT , " +
	                                          COLUMN_NAME_ORDER_DETAIL_QUANTITY + " TEXT )";

	public OrderDetailDatabase(Context context)
	{
		super(context, TABLE_NAME_ORDER_DETAIL, null, DATABASE_VERSION);
	}

	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL(createTable_order_detail);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ORDER_DETAIL);
		db.execSQL(createTable_order_detail);
	}

	public void addOrderDetail(OrderDetail orderDetail)
	{	
		SQLiteDatabase db = null;
		db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME_ORDER_DETAIL_ID_ORDER, orderDetail.getIdOrder());
        contentValues.put(COLUMN_NAME_ORDER_DETAIL_ID_PRODUCT, orderDetail.getIdProduct());
        contentValues.put(COLUMN_NAME_ORDER_DETAIL_UNIT_PRICE, orderDetail.getUnitPrice());
        contentValues.put(COLUMN_NAME_ORDER_DETAIL_QUANTITY, orderDetail.getQuantity());
		db.insert(TABLE_NAME_ORDER_DETAIL, null, contentValues);
		db.close();
	}
	
	public int updateOrderDetail(OrderDetail orderDetail)
	{
		SQLiteDatabase db = null;
		db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME_ORDER_DETAIL_ID_ORDER_DETAIL, orderDetail.getIdOrderDetail());
        contentValues.put(COLUMN_NAME_ORDER_DETAIL_ID_ORDER, orderDetail.getIdOrder());
        contentValues.put(COLUMN_NAME_ORDER_DETAIL_ID_PRODUCT, orderDetail.getIdProduct());
        contentValues.put(COLUMN_NAME_ORDER_DETAIL_UNIT_PRICE, orderDetail.getIdProduct());
        contentValues.put(COLUMN_NAME_ORDER_DETAIL_QUANTITY, orderDetail.getQuantity() );
		int ret = db.update(TABLE_NAME_ORDER_DETAIL, contentValues, COLUMN_NAME_ORDER_DETAIL_ID_ORDER_DETAIL + " = " + orderDetail.getIdOrderDetail(), null);
		db.close();
		return ret;
	}

	public List<OrderDetail> getOrderDetailsByOrderID(String orderID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
        String[] proyection =  new String[]
                {
                COLUMN_NAME_ORDER_DETAIL_ID_ORDER_DETAIL,
                COLUMN_NAME_ORDER_DETAIL_ID_ORDER,
                COLUMN_NAME_ORDER_DETAIL_ID_PRODUCT,
                COLUMN_NAME_ORDER_DETAIL_UNIT_PRICE,
                COLUMN_NAME_ORDER_DETAIL_QUANTITY
                };

        Cursor cursor = db.query(TABLE_NAME_ORDER_DETAIL, proyection, COLUMN_NAME_ORDER_DETAIL_ID_ORDER + " = ?", new String[]{orderID}, null, null, COLUMN_NAME_ORDER_DETAIL_ID_ORDER_DETAIL);
		List<OrderDetail> orderDetails = new ArrayList<OrderDetail>();
		
		if(cursor != null && cursor.getCount() != 0)
		{	
			if(cursor.moveToFirst()){
				try
                {
					do
                    {
                        String idOrderDetail = cursor.getString(0);
                        String idOrder = cursor.getString(1);
                        String idProduct = cursor.getString(2);
                        String unitPrice = cursor.getString(3);
                        String quantity = cursor.getString(4);

                        orderDetails.add(  new OrderDetail( idOrderDetail,  idOrder,  idProduct, unitPrice, quantity));
					}
                    while(cursor.moveToNext());
					
				}
                catch(Exception e )
                {
					e.printStackTrace();
				}
			} 
		}
		cursor.close();
		db.close();
		return orderDetails;
	}

	public int deleteOrderDetail(int idOrderDetail){
		SQLiteDatabase db = null;
		db = this.getWritableDatabase();
		int ret = db.delete(TABLE_NAME_ORDER_DETAIL, COLUMN_NAME_ORDER_DETAIL_ID_ORDER_DETAIL + " = " + idOrderDetail,  null );
		db.close();
		return ret;
	}

	@Override
	public Object getByName(String name)
	{
		return null;
	}

	@Override
	public Object getAll()
	{
		SQLiteDatabase db = this.getWritableDatabase();
		String[] proyection =  new String[]
				{
						COLUMN_NAME_ORDER_DETAIL_ID_ORDER_DETAIL,
						COLUMN_NAME_ORDER_DETAIL_ID_ORDER,
						COLUMN_NAME_ORDER_DETAIL_ID_PRODUCT,
						COLUMN_NAME_ORDER_DETAIL_UNIT_PRICE,
						COLUMN_NAME_ORDER_DETAIL_QUANTITY
				};

		Cursor cursor = db.query(TABLE_NAME_ORDER_DETAIL, proyection, null, null, null, null, COLUMN_NAME_ORDER_DETAIL_ID_ORDER_DETAIL);
		List<OrderDetail> orderDetails = new ArrayList<OrderDetail>();

		if(cursor != null && cursor.getCount() != 0)
		{
			if(cursor.moveToFirst()){
				try
				{
					do
					{
						String idOrderDetail = cursor.getString(0);
						String idOrder = cursor.getString(1);
						String idProduct = cursor.getString(2);
						String unitPrice = cursor.getString(3);
						String quantity = cursor.getString(4);

						orderDetails.add(  new OrderDetail( idOrderDetail,  idOrder,  idProduct, unitPrice, quantity));
					}
					while(cursor.moveToNext());

				}
				catch(Exception e )
				{
					e.printStackTrace();
				}
			}
		}
		cursor.close();
		db.close();
		return (Object) orderDetails;
	}
}
