package com.easypoint.app.easypoint.util;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.easypoint.app.easypoint.database.ProductDatabase;
import com.easypoint.app.easypoint.model.Product;

import java.util.ArrayList;
import java.util.List;


public class Configuration
{

	public static String FILES_DIR;
	private static Configuration configuration = null;
	private       SharedPreferences        prefs;
	private       SharedPreferences.Editor prefsEditor;
	public static ArrayList<String>        imageNames;
	private       Activity                 activity;

	public static       String PREFERENCES             = "PREFERENCES";
	public static final String PRODUCT_IMAGES_QUANTITY = "PRODUCT_IMAGES_QUANTITY";
	public static final String HOST                    = "http://192.168.1.13:80/EasyPoint";
	//= "http://easypoint.site90.com";
	public static final String DATA                    = "data";
	public static final String MESSAGE                 = "message";
	public static final String SUCCESS                 = "success";
	public static final String RESULT                  = "result";
	public static final String ENTIDAD                 = "entidad";
	public static final String TABLE_NAME              = "table_name";

	//ENTIDADES // METODOS DE CONSULTA EN LA DB
	public static final String GET_IMAGES_NAMES   = "get_images_names";
	public static final String GET_IMAGE_QUANTITY = "get_image_quantity";
	/*
	public static final String GET_IMAGES_NAMES = "get_images_names";

	public static final String GET_IMAGES_NAMES = "get_images_names";
	*/


	public static final String GET_ALL_PRODUCTS = "get_all_products";
	public static final String PRODUCTS         = "products";

	public static final String ID              = "id";
	public static final String CATEGORY        = "category";
	public static final String DESCRIPTION     = "description";
	public static final String SUPPLIER        = "supplierId";
	public static final String NET_WEIGHT      = "netWeight";
	public static final String COST            = "cost";
	public static final String PRICE           = "price";
	public static final String EXPIRE_DATE     = "expireDate";
	public static final String STOCK           = "stock";
	public static final String LOW_LIMIT_STOCK = "lowLimitStock";
	public static final String IMAGE_PATH      = "imagePath";

	public static final String IMAGE_DIRECTORY = "products";
	private static String quantity;
	private static String productImageDirectory;
	private static Product selectedProduct;

	public static void setSelectedProduct(Product selectedProduct)
	{
		Configuration.selectedProduct = selectedProduct;
	}

	public static Product getSelectedProduct()
	{
		return selectedProduct;
	}

	public List<Product> getProducts()
	{
		return products;
	}

	public boolean setProducts(List<Product> products)
	{
		this.products = products;
		return new ProductDatabase(activity).updateProducts(products);
	}

	private List<Product> products;

	private Configuration(Activity activity)
	{
		this.activity = activity;
	}

	public static String getQuantity()
	{
		return quantity;
	}

	public static void setCantidad(String quantity)
	{
		Configuration.quantity = quantity;
	}

	public static synchronized Configuration getInstance(Activity activity)
	{
		if (configuration == null)
		{
			configuration = new Configuration(activity);
			configuration.configureSharePreference();
			FILES_DIR = activity.getFilesDir().toString();
		}
		return configuration;
	}

	public SharedPreferences getPrefs()
	{
		return prefs;
	}

	public void setPrefs(SharedPreferences prefs)
	{
		this.prefs = prefs;
	}

	public SharedPreferences.Editor getPrefsEditor()
	{
		return prefsEditor;
	}

	public void setPrefsEditor(SharedPreferences.Editor prefsEditor)
	{
		this.prefsEditor = prefsEditor;
	}

	public void configureSharePreference()
	{
		this.setPrefs(activity.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE));
		this.setPrefsEditor(this.getPrefs().edit());
	}



	//Generic Methods

	public static void setUpFragment(Activity activity, int resourceID, FragmentManager childFragmentManager, Fragment fragment, Bundle bundle)
	{

		if(activity != null)
		{

			FragmentManager fragmentManager = childFragmentManager;
			if (bundle != null)
			{
				fragment.setArguments(bundle);
			}
			fragmentManager.beginTransaction().replace(resourceID, fragment).commit();
		} else
		{
			Log.e("E", "Activity null");
		}

	}
}
