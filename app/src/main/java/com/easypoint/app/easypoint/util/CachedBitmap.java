package com.easypoint.app.easypoint.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.easypoint.app.easypoint.model.Product;

import java.io.*;
import java.util.Date;

/**
 * Created by sterlingdiazd on 14/06/2015.
 */
public class CachedBitmap implements Serializable
{
	private static final long serialVersionUID = -6298516694275121291L;
	static transient Bitmap bitmap;

	public CachedBitmap()
	{
	}

	public static void writeObject(Activity activity, String fileName, Product product) throws IOException
	{
		try
		{
			FileOutputStream fileOutputStream = activity.openFileOutput(fileName, Context.MODE_PRIVATE);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			// This will serialize all fields that you did not mark with 'transient'
			// (Java's default behaviour)
			objectOutputStream.writeObject(product);
			// Now, manually serialize all transient fields that you want to be serialized
			bitmap = product.getBitmap();
			if (bitmap != null)
			{
				ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
				boolean success = bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
				if (success)
				{
					objectOutputStream.writeObject(byteStream.toByteArray());
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public static Product readObject(Activity activity, String fileName) throws IOException, ClassNotFoundException
	{
		Product product = null;
		try
		{
			// Now, all again, deserializing - in the SAME ORDER!
			// All non-transient fields

			FileInputStream fileInputStream = activity.openFileInput(fileName);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			product = (Product) objectInputStream.readObject();

			// All other fields that you serialized
			byte[] image = (byte[]) objectInputStream.readObject();
			if (image != null && image.length > 0)
			{
				bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
			}
			product.setBitmap(bitmap);
			objectInputStream.close();
			fileInputStream.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return product;
	}

}
