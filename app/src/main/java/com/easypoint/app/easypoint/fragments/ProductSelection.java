package com.easypoint.app.easypoint.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.adapters.AdapterGridView;
import com.easypoint.app.easypoint.database.ProductDatabase;
import com.easypoint.app.easypoint.model.Item;
import com.easypoint.app.easypoint.model.Product;
import com.easypoint.app.easypoint.util.Configuration;
import com.easypoint.app.easypoint.util.DataConfiguration;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductSelection.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductSelection#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductSelection extends Fragment implements TabHost.OnTabChangeListener,
                                                          AdapterView.OnItemClickListener
{
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_PARAM1 = "param1";
	private static final String ARG_PARAM2 = "param2";

	// TODO: Rename and change types of parameters
	private       String                        mParam1;
	private       String                        mParam2;
	private       OnFragmentInteractionListener mListener;
	public static TabHost                       tabs;
	private       TabHost.TabSpec               spec;
	private       GridView                      gridViewProducts;
	private       Activity                      activity;
	private       ImageView                     imageViewTest;
	private       Resources                     res;
	private       ArrayList<String>             imageNames;
	private       Configuration                 configuration;
	private       DataConfiguration             dataConfiguration;
	private       List<Product>                 products;
	private       ProductDatabase               productDatabase;

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param param1 Parameter 1.
	 * @param param2 Parameter 2.
	 * @return A new instance of fragment ProductSelection.
	 */
	// TODO: Rename and change types and number of parameters
	public static ProductSelection newInstance(String param1, String param2)
	{
		ProductSelection fragment = new ProductSelection();
		Bundle args = new Bundle();
		args.putString(ARG_PARAM1, param1);
		args.putString(ARG_PARAM2, param2);
		fragment.setArguments(args);
		return fragment;
	}

	public ProductSelection()
	{
		// Required empty public constructor
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		try
		{
			mListener = (OnFragmentInteractionListener) activity;
			this.activity = activity;

			Bundle bundle = getArguments();
			if (bundle != null)
			{
				products = (List<Product>) bundle.getSerializable("products");
			}
			else
			{
				productDatabase = new ProductDatabase(activity);
				products = (List<Product>) productDatabase.getAll();
			}


		}
		catch (ClassCastException e)
		{
			throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
		}
	}

	private ArrayList<String> generateImageNames(List<Product> products)
	{
		ArrayList<String> imageNames = new ArrayList<String>();
        for (Product product : products)
        {
	        imageNames.add(product.getImagePath());
            //imageNames.add(ProductImageManager.generateImageName(product)); //Use this method to put the name of the file and save it in imagePa
        }
        return imageNames;
    }





    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null)
        {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        configuration = Configuration.getInstance(activity);
        dataConfiguration = new DataConfiguration();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_product_selection, container, false);
        //tabs.setBackgroundResource( R.drawable.adapter_background_list_item );
        initComponent(view);
        configureGridView(products);
        configureTabHost(savedInstanceState);
        configureTabChanges();

        return view;
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

    }

    private void initComponent(View view)
    {
        try
        {
            res = getResources();
            tabs = (TabHost) view.findViewById(R.id.tabHost);
            gridViewProducts = (GridView) view.findViewById(R.id.gridViewLayout_producs);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void configureGridView(List<Product> products)
    {
        imageNames = generateImageNames(products);
        List<Item> items = dataConfiguration.configureProductItems(imageNames);
        AdapterGridView adapterGridView = new AdapterGridView(activity, items, products);
        gridViewProducts.setAdapter(adapterGridView);
        gridViewProducts.setOnItemClickListener(this);
        Configuration.getInstance(activity).setProducts(adapterGridView.getProducts());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        //Item item = (Item) parent.getSelectedItem();
        Product pro = products.get(position);
        mListener.onProductSelected(pro);
    }

    public class AsyncTaskLoadImages extends AsyncTask<String, Void, String>
    {


	    private ProgressDialog progressDialog;
	    private Activity       activity;
	    private String         imagePath;
	    private ImageView      imageView;

	    public AsyncTaskLoadImages(Activity activity)
	    {
		    this.activity = activity;
	    }

	    public AsyncTaskLoadImages()
	    {

	    }

	    @Override
	    protected void onPreExecute()
	    {
		    super.onPreExecute();
		    progressDialog = new ProgressDialog(activity);
		    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		    progressDialog.setMessage(activity.getResources().getString(R.string.downloading_images));
		    progressDialog.show();
	    }

	    @Override
	    protected String doInBackground(String... arg0)
	    {
		    configureGridView(products);

		    return "";
	    }


	    @Override
	    protected void onPostExecute(String path)
	    {
		    super.onPostExecute(path);
		    progressDialog.dismiss();

	    }

    }

	private void configureTabHost(Bundle savedInstanceState)
	{
		tabs.setup();
		//  TAB 1
		String mostSold = getResources().getString(R.string.most_sold);
		spec = tabs.newTabSpec(mostSold);
		spec.setContent(R.id.tab1);

		LayoutInflater inflater = activity.getLayoutInflater();
		View viewMostSold = inflater.inflate(R.layout.view_tabhost, null);
		TextView titleMostSold = (TextView) viewMostSold.findViewById(R.id.textViewTitleTab);
		titleMostSold.setText(mostSold);
		FrameLayout frameImageMostSold = (FrameLayout) viewMostSold.findViewById(R.id.frame_image_tabhost);
		frameImageMostSold.setBackground(getResources().getDrawable(R.drawable.ic_drawer));
		spec.setIndicator(viewMostSold);


		tabs.addTab(spec);
		//  TAB 2
		String categories = getResources().getString(R.string.categories);
		spec = tabs.newTabSpec(categories);
		spec.setContent(R.id.tab2);


		View viewCategories = inflater.inflate(R.layout.view_tabhost, null);
		TextView titleCategories = (TextView) viewCategories.findViewById(R.id.textViewTitleTab);
		titleCategories.setText(categories);
		FrameLayout frameImageCategories = (FrameLayout) viewCategories.findViewById(R.id.frame_image_tabhost);
		frameImageCategories.setBackground(getResources().getDrawable(R.drawable.ic_drawer));
		spec.setIndicator(viewCategories);
		tabs.addTab(spec);

		tabs.setCurrentTab(0);
	}

	private void configureTabChanges()
	{
		tabs.setOnTabChangedListener(this);
	}

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(Uri uri)
	{
		if (mListener != null)
		{
			//mListener.onProductSelected(product);
		}
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		mListener = null;
	}

	@Override
	public void onTabChanged(String tabId)
	{

	}


	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener
	{
		// TODO: Update argument type and name
		void onProductSelected(Product product);
	}
}
