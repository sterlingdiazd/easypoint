package com.easypoint.app.easypoint.image_processing;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.easypoint.app.easypoint.util.Configuration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sterlingdiazd on 4/18/2014.
 */
public class ImageDownloader extends AsyncTask<String, Void, String>
{

	private Activity       activity;
	private String         filepath;
	private String         path;
	public  ProgressDialog progressDialog;
	private List<String>   pathValues;
	private String storagePath = "/data/data/com.compage.easy_point_of_sales/files/products/1-leche_milex.jpg";


	public ImageDownloader(Activity activity)
	{
		this.activity = activity;
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();
        /*
        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(activity.getResources().getString(R.string.downloading_images));
        progressDialog.show();
        */
		pathValues = new ArrayList<String>();
	}

	@Override
	protected String doInBackground(String... arg0)
	{
		String path = downloadImagesToSdCard(arg0[0], arg0[1]);

		return path;
    }

    public String downloadImagesToSdCard(String downloadUrl, String imageName)
    {
        try
        {
            URL url = new URL(downloadUrl);

            //String sdCard=Environment.getExternalStorageDirectory().toString();
            File myDir = new File(activity.getFilesDir(), Configuration.IMAGE_DIRECTORY);

                        /*  if specified not exist create new */
            if(!myDir.exists())
            {
                myDir.mkdir();
                Log.v("", "inside mkdir");
            }

                        /* checks the file and if it already exist delete */
            String fname = imageName;
            File file = new File(myDir, fname);
            filepath = file.getAbsolutePath();
            if (!file.exists ())
            {
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection)ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK)
                {
                    inputStream = httpConn.getInputStream();
                }

                FileOutputStream fos = new FileOutputStream(file);
                int totalSize = httpConn.getContentLength();
                int downloadedSize = 0;
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                while ( (bufferLength = inputStream.read(buffer)) >0 )
                {
                    fos.write(buffer, 0, bufferLength);
                    downloadedSize += bufferLength;
                    Log.i("Progress:", "downloadedSize:" + downloadedSize + "totalSize:" + totalSize) ;
                }

                fos.close();

            }
        }
        catch(IOException io)
        {
            io.printStackTrace();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return filepath;
    }


    @Override
    protected void onPostExecute(String path)
    {
        super.onPostExecute(path);
        if(path != null)
        {
            pathValues.add(path);
        }
        Log.e("PATH", path);
    }


    public List<String> getPathValues() {
        return pathValues;
    }
}