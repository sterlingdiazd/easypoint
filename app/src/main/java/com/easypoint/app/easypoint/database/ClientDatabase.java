package com.easypoint.app.easypoint.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.easypoint.app.easypoint.model.Client;
import com.easypoint.app.easypoint.model.Order;
import com.easypoint.app.easypoint.model.OrderDetail;

import java.util.ArrayList;
import java.util.List;


public class ClientDatabase extends SQLiteOpenHelper implements SearchableDatabase
{

	public static final  String TABLE_NAME_CLIENT                  = "Client";
	public static final  String COLUMN_NAME_CLIENT_ID              = "id";
	public static final  String COLUMN_NAME_CLIENT_CEDULA          = "cedula";
	public static final  String COLUMN_NAME_CLIENT_NOMBRE          = "nombre";
	public static final  String COLUMN_NAME_CLIENT_APELLIDO        = "apellido";
	public static final  String COLUMN_NAME_CLIENT_EMAIL           = "email";
	public static final  String COLUMN_NAME_CLIENT_DIRECCION       = "direccion";
	public static final  String COLUMN_NAME_CLIENT_REGID           = "regid";
	public static final  String COLUMN_NAME_CLIENT_GEOLOCALIZACION = "geolocalizacion";
	public static final  String COLUMN_NAME_CLIENT_BIRTHDATE       = "birthdate";
	public static final  String COLUMN_NAME_CLIENT_CELLPHONE       = "cellphone";
	private static final int    DATABASE_VERSION                   = 1;
	private Context context;

	private String createTable_client = "create table " +
	                                    TABLE_NAME_CLIENT + " ( " +
	                                    COLUMN_NAME_CLIENT_ID + " integer primary key autoincrement , " +
	                                    COLUMN_NAME_CLIENT_CEDULA + " TEXT , " +
	                                    COLUMN_NAME_CLIENT_NOMBRE + " TEXT , " +
	                                    COLUMN_NAME_CLIENT_APELLIDO + " TEXT , " +
	                                    COLUMN_NAME_CLIENT_EMAIL + " TEXT, " +
	                                    COLUMN_NAME_CLIENT_DIRECCION + " TEXT, " +
	                                    COLUMN_NAME_CLIENT_REGID + " TEXT, " +
	                                    COLUMN_NAME_CLIENT_GEOLOCALIZACION + " TEXT , " +
	                                    COLUMN_NAME_CLIENT_BIRTHDATE + " TEXT, " +
	                                    COLUMN_NAME_CLIENT_CELLPHONE + " TEXT )";

	public ClientDatabase(Context context)
	{
		super(context, TABLE_NAME_CLIENT, null, DATABASE_VERSION);
		this.context = context;
	}

	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL(createTable_client);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_CLIENT);
		db.execSQL(createTable_client);
	}

	public boolean addClient(Client client)
	{
		SQLiteDatabase db = null;
        db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME_CLIENT_CEDULA, client.getCedula());
        contentValues.put(COLUMN_NAME_CLIENT_NOMBRE, client.getName());
        contentValues.put(COLUMN_NAME_CLIENT_APELLIDO, client.getLastname());
        contentValues.put(COLUMN_NAME_CLIENT_EMAIL, client.getEmail());
        contentValues.put(COLUMN_NAME_CLIENT_DIRECCION, client.getDireccion());
        contentValues.put(COLUMN_NAME_CLIENT_REGID, client.getRegid());
        contentValues.put(COLUMN_NAME_CLIENT_GEOLOCALIZACION, client.getGeolocalizacion());
        contentValues.put(COLUMN_NAME_CLIENT_BIRTHDATE, client.getBirthdate());
        contentValues.put(COLUMN_NAME_CLIENT_CELLPHONE, client.getCellphone());
        long insertResult = db.insert(TABLE_NAME_CLIENT, null, contentValues);
        if(db != null)
        {
            db.close();
        }
        if(insertResult == -1)
        {
            return false;
        } else {
            return true;
        }

    }

    public int updateClient(Client client)
    {
        SQLiteDatabase db = null;
        db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME_CLIENT_CEDULA, client.getCedula());
        contentValues.put(COLUMN_NAME_CLIENT_NOMBRE, client.getName());
        contentValues.put(COLUMN_NAME_CLIENT_APELLIDO, client.getLastname());
        contentValues.put(COLUMN_NAME_CLIENT_EMAIL, client.getEmail());
        contentValues.put(COLUMN_NAME_CLIENT_DIRECCION, client.getDireccion());
        contentValues.put(COLUMN_NAME_CLIENT_REGID, client.getRegid());
        contentValues.put(COLUMN_NAME_CLIENT_GEOLOCALIZACION, client.getGeolocalizacion());
        contentValues.put(COLUMN_NAME_CLIENT_BIRTHDATE, client.getBirthdate());
        contentValues.put(COLUMN_NAME_CLIENT_CELLPHONE, client.getCellphone());
        int ret = db.update(TABLE_NAME_CLIENT, contentValues, COLUMN_NAME_CLIENT_ID + " = " + client.getId(), null);
        db.close();
        return ret;
    }

    public Client getClientByPersonalID(String personalID)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] proyection = new String[]{COLUMN_NAME_CLIENT_ID, COLUMN_NAME_CLIENT_CEDULA, COLUMN_NAME_CLIENT_NOMBRE, COLUMN_NAME_CLIENT_APELLIDO, COLUMN_NAME_CLIENT_EMAIL, COLUMN_NAME_CLIENT_DIRECCION, COLUMN_NAME_CLIENT_REGID, COLUMN_NAME_CLIENT_GEOLOCALIZACION, COLUMN_NAME_CLIENT_BIRTHDATE, COLUMN_NAME_CLIENT_CELLPHONE};
        Client client = null;
        try
        {
            Cursor cursor = db.query(TABLE_NAME_CLIENT, proyection, COLUMN_NAME_CLIENT_CEDULA + " = ?", new String[]{personalID}, null, null, COLUMN_NAME_CLIENT_ID);

            if (cursor != null && cursor.getCount() != 0)
            {
                if (cursor.moveToFirst())
                {
                    do
                    {
                        String id = cursor.getString(0);
                        String cedula = cursor.getString(1);
                        String nombre = cursor.getString(2);
                        String apellido = cursor.getString(3);
                        String email = cursor.getString(4);
                        String direccion = cursor.getString(5);
                        String regid = cursor.getString(6);
                        String geolocalizacion = cursor.getString(7);
                        String birthdate = cursor.getString(8);
                        String cellphone = cursor.getString(9);

                        client = new Client(id, cedula, nombre, apellido, email, direccion, regid, geolocalizacion, birthdate, cellphone);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
            db.close();

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return client;
    }





    public int deleteClient(int idClient)
    {
        SQLiteDatabase db = null;
        db = this.getWritableDatabase();
        int ret = db.delete(TABLE_NAME_CLIENT, COLUMN_NAME_CLIENT_ID + " = " + idClient, null);
        db.close();
        return ret;
    }

    public int calculateDebt(String idClient)
    {
        int productTotal = 0;
        List<Order> orders = new OrderDatabase(context).getOrdesrByClient(idClient);
        for (Order order : orders)
        {
            List<OrderDetail> details = order.getOrderDetails();
            for (OrderDetail detail : details)
            {
                productTotal += Integer.valueOf(detail.getQuantity()) * Integer.valueOf(detail.getUnitPrice());
            }
        }
        return productTotal;
    }

    @Override
    public Object getByName(String name)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] proyection = new String[]{COLUMN_NAME_CLIENT_ID, COLUMN_NAME_CLIENT_CEDULA, COLUMN_NAME_CLIENT_NOMBRE, COLUMN_NAME_CLIENT_APELLIDO, COLUMN_NAME_CLIENT_EMAIL, COLUMN_NAME_CLIENT_DIRECCION, COLUMN_NAME_CLIENT_REGID, COLUMN_NAME_CLIENT_GEOLOCALIZACION, COLUMN_NAME_CLIENT_BIRTHDATE, COLUMN_NAME_CLIENT_CELLPHONE};
        List<Client> clients = new ArrayList<Client>();
        try
        {
            Cursor cursor = db.query(TABLE_NAME_CLIENT, proyection, COLUMN_NAME_CLIENT_NOMBRE + " like ?", new String[]{"%" + name + "%"}, null, null, COLUMN_NAME_CLIENT_ID);

            if (cursor != null && cursor.getCount() != 0)
            {
                if (cursor.moveToFirst())
                {
                    do
                    {
                        String id = cursor.getString(0);
                        String cedula = cursor.getString(1);
                        String nombre = cursor.getString(2);
                        String apellido = cursor.getString(3);
                        String email = cursor.getString(4);
                        String direccion = cursor.getString(5);
                        String regid = cursor.getString(6);
                        String geolocalizacion = cursor.getString(7);
                        String birthdate = cursor.getString(8);
                        String cellphone = cursor.getString(9);

                        clients.add(new Client(id, cedula, nombre, apellido, email, direccion, regid, geolocalizacion, birthdate, cellphone));
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
            db.close();

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return (Object) clients;
    }

    public Object getAll()
    {
        SQLiteDatabase db = getWritableDatabase();
        String[] proyection = new String[]{COLUMN_NAME_CLIENT_ID, COLUMN_NAME_CLIENT_CEDULA, COLUMN_NAME_CLIENT_NOMBRE, COLUMN_NAME_CLIENT_APELLIDO, COLUMN_NAME_CLIENT_EMAIL, COLUMN_NAME_CLIENT_DIRECCION, COLUMN_NAME_CLIENT_REGID, COLUMN_NAME_CLIENT_GEOLOCALIZACION, COLUMN_NAME_CLIENT_BIRTHDATE, COLUMN_NAME_CLIENT_CELLPHONE};

        Cursor cursor = db.query(TABLE_NAME_CLIENT, proyection, null, null, null, null, COLUMN_NAME_CLIENT_ID);

        List<Client> clients = new ArrayList<Client>();

        if (cursor != null && cursor.getCount() != 0)
        {
            if (cursor.moveToFirst())
            {
                try
                {
                    do
                    {
                        String id = cursor.getString(0);
                        String cedula = cursor.getString(1);
                        String nombre = cursor.getString(2);
                        String apellido = cursor.getString(3);
                        String email = cursor.getString(4);
                        String direccion = cursor.getString(5);
                        String regid = cursor.getString(6);
                        String geolocalizacion = cursor.getString(7);
                        String birthdate = cursor.getString(8);
                        String cellphone = cursor.getString(9);

                        clients.add(new Client(id, cedula, nombre, apellido, email, direccion, regid, geolocalizacion, birthdate, cellphone));
                    } while (cursor.moveToNext());

                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        cursor.close();
        db.close();
        return (Object) clients;
    }
}
