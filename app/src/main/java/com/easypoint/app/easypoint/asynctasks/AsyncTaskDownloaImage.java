package com.easypoint.app.easypoint.asynctasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.util.Configuration;

import org.json.JSONArray;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by sterlingdiazd on 23/01/2015.
 */
public class AsyncTaskDownloaImage extends AsyncTask<String, Integer, Boolean>
{

	public ProgressDialog progressDialog;
	private String responseMessage;
	private Activity activity;
	private JSONArray data;

	private Resources res;


	public AsyncTaskDownloaImage(Activity activity)
	{
		this.activity = activity;
		res = activity.getResources();
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();
		progressDialog = new ProgressDialog(activity);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage(res.getString(R.string.downloading_images));
		progressDialog.show();
	}

	public Boolean doInBackground(String... params)
	{

		boolean operationWasSuccessfull = false;

		int image_quantity = Configuration.imageNames.size();

		Double preloops = Double.valueOf(image_quantity +"");
		int loops = 0;

		if(preloops > 0)
		{
			try
			{
				String doubleValue =  preloops.toString(); //.parseInt(preloops+"");
				int start = doubleValue.indexOf(".");
				int end = doubleValue.length();

				String decimalValue = doubleValue.substring(start+1, end);
				int decimalInt = Integer.valueOf(decimalValue);
				if(decimalInt > 0)
				{
					loops = 	preloops.intValue() + 1;
				} else {
					loops = preloops.intValue();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();

			}
		} else {
			loops = 1;
		}

		for (int x = 0; x < loops; x += 10)
		{
			try
			{
				String imageName1 = Configuration.imageNames.get(x);
				String imageUrl1 = res.getString(R.string.hostURL) + "/images/" + imageName1;
				//new ImageDownloader(activity).execute(imageUrl1, imageName1);
				downloadImagesToSdCard(activity, imageUrl1, imageName1);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			try
			{
				String imageName2 = Configuration.imageNames.get(x + 1);
				String imageUrl2 = res.getString(R.string.hostURL) + "/images/" + imageName2;
				// new ImageDownloader(activity).execute(imageUrl2, imageName2);
				downloadImagesToSdCard(activity, imageUrl2, imageName2);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			try
			{
				String imageName3 = Configuration.imageNames.get(x + 2);
				String imageUrl3 = res.getString(R.string.hostURL) + "/images/" + imageName3;
				//new ImageDownloader(activity).execute(imageUrl3, imageName3);
				downloadImagesToSdCard(activity, imageUrl3, imageName3);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			try
			{
				String imageName4 = Configuration.imageNames.get(x + 3);
				String imageUrl4 = res.getString(R.string.hostURL) + "/images/" + imageName4;
				//new ImageDownloader(activity).execute(imageUrl4, imageName4);
				downloadImagesToSdCard(activity, imageUrl4, imageName4);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			try
			{
				String imageName5 = Configuration.imageNames.get(x + 4);
				String imageUrl5 = res.getString(R.string.hostURL) + "/images/" + imageName5;
				//new ImageDownloader(activity).execute(imageUrl5, imageName5);
				downloadImagesToSdCard(activity, imageUrl5, imageName5);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			try
			{
				String imageName6 = Configuration.imageNames.get(x + 5);
				String imageUrl6 = res.getString(R.string.hostURL) + "/images/" + imageName6;
				//  new ImageDownloader(activity).execute(imageUrl6, imageName6);
				downloadImagesToSdCard(activity, imageUrl6, imageName6);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			try
			{
				String imageName7 = Configuration.imageNames.get(x + 6);
				String imageUrl7 = res.getString(R.string.hostURL) + "/images/" + imageName7;
				//new ImageDownloader(activity).execute(imageUrl7, imageName7);
				downloadImagesToSdCard(activity, imageUrl7, imageName7);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			try
			{
				String imageName8 = Configuration.imageNames.get(x + 7);
				String imageUrl8 = res.getString(R.string.hostURL) + "/images/" + imageName8;
				//new ImageDownloader(activity).execute(imageUrl8, imageName8);
				downloadImagesToSdCard(activity, imageUrl8, imageName8);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			try
			{
				String imageName9 = Configuration.imageNames.get(x + 8);
				String imageUrl9 = res.getString(R.string.hostURL) + "/images/" + imageName9;
				//new ImageDownloader(activity).execute(imageUrl9, imageName9);
				downloadImagesToSdCard(activity, imageUrl9, imageName9);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			try
			{
				String imageName10 = Configuration.imageNames.get(x + 9);
				String imageUrl10 = res.getString(R.string.hostURL) + "/images/" + imageName10;
				//new ImageDownloader(activity).execute(imageUrl10, imageName10);
				downloadImagesToSdCard(activity, imageUrl10, imageName10);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

		}

		for (String imageName : Configuration.imageNames)
		{
			//UrlImageViewHelper.loadUrlDrawable(activity, imageUrl);
		}

		operationWasSuccessfull = true;


		return operationWasSuccessfull;
	}

	public String downloadImagesToSdCard(Activity activity, String downloadUrl, String imageName)
	{
		String filepath = "";
		String path;
		List<String> pathValues;

		try
		{
			URL url = new URL(downloadUrl);

			//String sdCard=Environment.getExternalStorageDirectory().toString();
			File myDir = new File(activity.getFilesDir(), Configuration.IMAGE_DIRECTORY);


			if (!myDir.exists())
			{
				myDir.mkdir();
				Log.v("", "inside mkdir");
			}


			String fname = imageName;
			File file = new File(myDir, fname);
			filepath = file.getAbsolutePath();
			if (!file.exists())
			{
				URLConnection ucon = url.openConnection();
				InputStream inputStream = null;
				HttpURLConnection httpConn = (HttpURLConnection) ucon;
				httpConn.setRequestMethod("GET");
				httpConn.connect();

				if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK)
				{
					inputStream = httpConn.getInputStream();
				}

				FileOutputStream fos = new FileOutputStream(file);
				int totalSize = httpConn.getContentLength();
				int downloadedSize = 0;
				byte[] buffer = new byte[1024];
				int bufferLength = 0;
				while ((bufferLength = inputStream.read(buffer)) > 0)
				{
					fos.write(buffer, 0, bufferLength);
					downloadedSize += bufferLength;
					Log.i("Progress:", "downloadedSize:" + downloadedSize + "totalSize:" + totalSize);
				}

				fos.close();

			}
		} catch (IOException io)
		{
			io.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return filepath;
	}

	protected void onPostExecute(Boolean result)
	{
		progressDialog.dismiss();

		if (result)
		{
			Toast.makeText(activity, res.getString(R.string.images_downloaded), Toast.LENGTH_LONG).show();
			Configuration configuration = Configuration.getInstance(activity);
			//Cambiar la cantidad de imagenes descargadas, por la cantidad de archivos que hay en la carpeta.
			configuration.getPrefsEditor().putInt(Configuration.PRODUCT_IMAGES_QUANTITY, Configuration.imageNames.size());
			configuration.getPrefsEditor().commit();

			AsyncTaskDownloadProducts asyncTaskDownloadProducts = new AsyncTaskDownloadProducts(activity);
			asyncTaskDownloadProducts.execute(Configuration.GET_ALL_PRODUCTS, Configuration.PRODUCTS);
		}
	}


}
