package com.easypoint.app.easypoint.util;

import android.app.Activity;

import com.easypoint.app.easypoint.database.OrderDatabase;
import com.easypoint.app.easypoint.database.SearchableDatabase;
import com.easypoint.app.easypoint.model.Client;
import com.easypoint.app.easypoint.model.Order;
import com.easypoint.app.easypoint.model.OrderDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sterlingdiazd on 03/05/2015.
 */
public class EntitySearches
{
	public Object searchEntities(String query, Object objectList, SearchableDatabase database)
	{
		List<Object> clients = null;
		if(query.length() > 0)
		{
			objectList = database.getByName(query.toString().trim());
		}
		else {
			objectList = database.getAll();
		}
		return objectList;
	}

	public List<OrderDetail> retrieveOrderDataForClient(Activity activity, Client client)
	{
		List<Order> orders = new OrderDatabase(activity).getOrdesrByClient(client.getId());

		List<OrderDetail> details = new ArrayList<OrderDetail>();
		for (Order order : orders)
		{
			if (order != null)
			{

				List<OrderDetail> orderDetails = order.getOrderDetails();
				if (orderDetails != null)
				{
					for (OrderDetail orderDetail : orderDetails)
					{
						if (orderDetail != null)
							details.add(orderDetail);
					}
				}
			}
		}
		return details;
	}
}
