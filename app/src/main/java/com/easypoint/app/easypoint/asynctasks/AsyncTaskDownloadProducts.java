package com.easypoint.app.easypoint.asynctasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.database.ProductDatabase;
import com.easypoint.app.easypoint.fragments.ProductSelection;
import com.easypoint.app.easypoint.model.Product;
import com.easypoint.app.easypoint.util.Configuration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sterlingdiazd on 23/01/2015.
 */
public class AsyncTaskDownloadProducts extends AsyncTask<String, Integer, Boolean>
{

	public  ProgressDialog     progressDialog;
	//private Configuration configuration;
	private String             responseMessage;
	private Activity           activity;
	private ArrayList<Product> products;
	private String          data;

	public AsyncTaskDownloadProducts(Activity activity)
	{
		this.activity = activity;
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();
		progressDialog = new ProgressDialog(activity);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage(activity.getResources().getString(R.string.download_products));
		progressDialog.show();
	}

	public Boolean doInBackground(String... params)
	{

		boolean operationWasSuccessfull = false;

		try
		{

			ArrayList<NameValuePair> parametros = new ArrayList<NameValuePair>();
			parametros.add(new BasicNameValuePair(Configuration.ENTIDAD, params[0]));
			parametros.add(new BasicNameValuePair(Configuration.TABLE_NAME, params[1]));

			HttpPost post = new HttpPost(Configuration.HOST + "/Controllers/entry_point.php");
			try
			{
				post.setEntity(new UrlEncodedFormEntity(parametros));

				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse resp = null;
				try
				{
					resp = httpClient.execute(post);
					int status = resp.getStatusLine().getStatusCode();

					if (status == 200)
					{
						HttpEntity httpEntity = resp.getEntity();

						InputStream is = null;
						try
						{
							is = httpEntity.getContent();
						} catch (IOException e)
						{
							e.printStackTrace();
						}
						if (is != null)
						{
							BufferedReader rd = null;
							try
							{
								rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
							} catch (UnsupportedEncodingException e)
							{
								e.printStackTrace();
							}
							StringBuilder sb = new StringBuilder();
							String line = "";
							try
							{
								while ((line = rd.readLine()) != null)
								{
									sb.append(line + "\n");
								}
								is.close();

								String result = sb.toString();
								JSONObject o = new JSONObject(result);
								responseMessage = o.getString(Configuration.MESSAGE);

								if (o.getString(Configuration.RESULT).equalsIgnoreCase(Configuration.SUCCESS))
								{
									data = o.getString(Configuration.DATA);
									products = new Gson().fromJson(data.trim(), new TypeToken<List<Product>>(){}.getType());

									//Save in database
									ProductDatabase productDatabase = new ProductDatabase(activity);
									for (Product product : products)
									{
										productDatabase.addProduct(product);
									}

									operationWasSuccessfull = true;
								}

							} catch (IOException e)
							{
								e.printStackTrace();
							}
						}
					} else
					{
						return operationWasSuccessfull;
					}
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
			catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}




		} catch (Exception ex)
		{
			ex.printStackTrace();
			Log.e("ServicioRest", "Error: " + ex.getMessage() + "");
		}

		return operationWasSuccessfull;
	}

	protected void onPostExecute(Boolean result)
	{
		progressDialog.dismiss();
		Toast.makeText(activity, responseMessage, Toast.LENGTH_LONG).show();

		ProductSelection productSelection = new ProductSelection();
		Bundle bundle = null;
		Configuration.setUpFragment(activity,R.id.dashboard_product_selection, activity.getFragmentManager(), productSelection, bundle );
	}
	/*
	public ArrayList<Product> convertDataToProducts(JSONArray jsonArray)
	{
		ArrayList<Product> products = new ArrayList<Product>();


		for (int x = 0; x < jsonArray.length(); x++)
		{
			try
			{
				JSONObject obj = (JSONObject) jsonArray.get(x);
				String id = obj.getString(Configuration.ID);
				String category = obj.getString(Configuration.CATEGORY);
				String description = obj.getString(Configuration.DESCRIPTION);
				String supplier = obj.getString(Configuration.SUPPLIER);
				String net_weight = obj.getString(Configuration.NET_WEIGHT);
				String cost = obj.getString(Configuration.COST);
				String price = obj.getString(Configuration.PRICE);
				String expire_date = obj.getString(Configuration.EXPIRE_DATE);
				String stock = obj.getString(Configuration.STOCK);
				String low_limit_stock = obj.getString(Configuration.LOW_LIMIT_STOCK);
				String image_path = obj.getString(Configuration.IMAGE_PATH);

				products.add(new Product(id, category, description, supplier, net_weight, unitType cost, price, expire_date, stock, low_limit_stock,
						image_path));
			} catch (JSONException jsonException)
			{
				jsonException.printStackTrace();
			}
		}
		return products;
	}
	*/
}