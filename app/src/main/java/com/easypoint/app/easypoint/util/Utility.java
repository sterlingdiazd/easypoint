package com.easypoint.app.easypoint.util;

import android.widget.BaseAdapter;
import android.widget.ListView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utility {

	private String hyphen = "-";
	private Calendar calendar;
	private Date     date;

	public Utility()
	{
		super();
		calendar = Calendar.getInstance();
	}

	public static void fillupdateList(ListView listview, BaseAdapter adapter)
	{
		listview.setAdapter(adapter);
		listview.refreshDrawableState();
	}
	public String getDateTime()
	{
		return getCurrentDate() + " " + getCurrentTime();
	}

	public String getCurrentDate() {
		return createDateWithHyphen(calendar.get(Calendar.YEAR),
				(calendar.get(Calendar.MONTH) + 1),
				calendar.get(Calendar.DAY_OF_MONTH));
	}

	public String getCurrentTime() {
		String AM_PM = "";
		if(calendar.get(Calendar.AM_PM) == 0){
			AM_PM = "AM";
		} else {
			AM_PM = "PM";
		}
		return calendar.get(Calendar.HOUR) + ":"
				+ calendar.get(Calendar.MINUTE) + " " + AM_PM;
	}

	public String createDateWithHyphen(int year, int month, int day) {
		String mm = "";
		String dd = "";
		if (month < 10) {
			mm = "0" + month;
		} else {
			mm = "" + month;
		}
		if (day < 10) {
			dd = "0" + day;
		} else {
			dd = "" + day;
		}
		return year + hyphen + mm + hyphen + dd;
	}

	public String getRidOfHyphenInDate(String dateWithHyphen) {
		if (dateWithHyphen != null) {
			String dateWithoutWyphen = "";
			String year = dateWithHyphen.substring(0, 4);
			String month = dateWithHyphen.substring(5, 7);
			String day = dateWithHyphen.substring(8, 10);
			dateWithoutWyphen = year + month + day;
			return dateWithoutWyphen;
		} else {
			return "";
		}
	}

	public String createDateWithoutHyphen(int year, int month, int day) {
		return year + "0" + month + day;
	}

	public String separateDateByHiphen(String date) {
		String year = date.substring(0, 4);
		String month = date.substring(4, 6);
		String day = date.substring(6);

		return year + hyphen + month + hyphen + day;
	}

	public String transformDate(String stringDate) {
		 String separateDateByHiphenDate = separateDateByHiphen(stringDate);
		 SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		 SimpleDateFormat longDateFormat = new SimpleDateFormat("MMMM dd, yyyy");
		 Date date;
		 String convertedDate = null;
		 try {			
			date = simpleDateFormat.parse(separateDateByHiphenDate);
			convertedDate = longDateFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return convertedDate;
	}

}
