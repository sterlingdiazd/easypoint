package com.easypoint.app.easypoint.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.adapters.AdapterListViewFragmentProducts;
import com.easypoint.app.easypoint.database.ProductDatabase;
import com.easypoint.app.easypoint.model.Product;
import com.easypoint.app.easypoint.process_management.AlertDialogManager;
import com.easypoint.app.easypoint.util.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.easypoint.app.easypoint.util.Configuration.*;


public class FragmentProduct extends Fragment implements TabHost.OnTabChangeListener,
														 AdapterView.OnItemClickListener, View.OnClickListener, DatePickerDialog.OnDateSetListener,
														 View.OnFocusChangeListener, TextView.OnEditorActionListener, TextWatcher
{
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_PARAM1        = "param1";
	private static final String ARG_PARAM2        = "param2";
	private static       int    PRODUCT_CREATE    = 1;
	private static       int    PRODUCT_UPDATE    = 2;

	// TODO: Rename and change types of parameters
	private       String                        mParam1;
	private       String                        mParam2;
	private       OnFragmentInteractionListener mListener;
	public static TabHost                       tabs;
	private       TabHost.TabSpec               spec;
	private       ListView                      listViewFragmentProduct;
	private       Activity                      activity;
	private       ImageView                     imageViewTest;
	private       Resources                     res;
	private       ArrayList<String>             imageNames;
	private       Configuration                 configuration;
	private       DataConfiguration             dataConfiguration;
	private       List<Product>                 products;
	private       ProductDatabase               productDatabase;
	private       LinearLayout                  linearLayoutCreateProduct;
	private static final int SELECT_PICTURE = 1;
	private String             selectedImagePath;
	private ImageButton        imageButtonFPselectImage;
	private EditText           editTextFPCategory;
	private EditText           editTextFPDescription;
	private EditText           editTextFPSupplier;
	private EditText           editTextFPCost;
	private EditText           editTextFPPrice;
	private EditText           editTextFPExpiration;
	private EditText           editTextFPStock;
	private EditText           editTextFPStockLimit;
	private List<View>         fields;
	private DatePickerFragment datePickerFragment;
	private Fragment           fragmentActivity;
	private Utility            utility;
	private View               currentSelectedView;
	private Product            selectedProduct;

	private boolean             isEditingProduct;
	private boolean             isItemSelected = false;
	private ProductImageManager productImageManager;
	private AdapterListViewFragmentProducts adapterListViewFragmentProducts;

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param param1 Parameter 1.
	 * @param param2 Parameter 2.
	 * @return A new instance of fragment ProductSelection.
	 */
	// TODO: Rename and change types and number of parameters
	public static FragmentProduct newInstance(String param1, String param2)
	{
		FragmentProduct fragment = new FragmentProduct();
		Bundle          args     = new Bundle();
		args.putString(ARG_PARAM1, param1);
		args.putString(ARG_PARAM2, param2);
		fragment.setArguments(args);
		return fragment;
	}

	public FragmentProduct()
	{
		// Required empty public constructor
	}

	@Override
	public void onAttach(Activity activity)
	{
		this.activity = activity;
		super.onAttach(activity);
		try
		{
			mListener = (OnFragmentInteractionListener) activity;

			productDatabase = new ProductDatabase(activity);

			Bundle bundle = getArguments();
			if (bundle != null && bundle.size() > 0)
			{
				products = (List<Product>) bundle.getSerializable("products");
			}
			else
			{
				products = (List<Product>) productDatabase.getAll();
			}


		}
		catch (ClassCastException e)
		{
			throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		if (getArguments() != null)
		{
			mParam1 = getArguments().getString(ARG_PARAM1);
			mParam2 = getArguments().getString(ARG_PARAM2);
		}

		configuration = getInstance(activity);
		dataConfiguration = new DataConfiguration();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		//super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.inventory, menu);

		SearchView search = (SearchView) menu.findItem(R.id.action_search).getActionView();
		search.setQueryHint(getString(R.string.search_product));
		search.setOnQueryTextListener(queryListener);

		int id = search.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);

		TextView text = (TextView) search.findViewById(id);
		text.setHintTextColor(Color.WHITE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_product, container, false);
		//tabs.setBackgroundResource( R.drawable.adapter_background_list_item );
		initComponent(view);
		configureView(products);
		setHasOptionsMenu(true);
		//configureTabHost(savedInstanceState);
		// configureTabChanges();
		return view;
	}


	private void initComponent(View view)
	{
		try
		{
			res = getResources();
			utility = new Utility();
			//tabs = (TabHost) view.findViewById(R.id.tabHost);
			listViewFragmentProduct = (ListView) view.findViewById(R.id.listViewFragmentProduct);
			linearLayoutCreateProduct = (LinearLayout) view.findViewById(R.id.linearLayoutCreateProduct);
			changeVisibility(linearLayoutCreateProduct);

			imageButtonFPselectImage = (ImageButton) view.findViewById(R.id.imageButtonFPselectImage);
			editTextFPCategory = (EditText) view.findViewById(R.id.editTextFPCategory);
			editTextFPDescription = (EditText) view.findViewById(R.id.editTextFPDescription);
			editTextFPSupplier = (EditText) view.findViewById(R.id.editTextFPSupplier);
			editTextFPCost = (EditText) view.findViewById(R.id.editTextFPCost);
			editTextFPPrice = (EditText) view.findViewById(R.id.editTextFPPrice);
			editTextFPExpiration = (EditText) view.findViewById(R.id.editTextFPExpiration);
			editTextFPStock = (EditText) view.findViewById(R.id.editTextFPStock);
			editTextFPStockLimit = (EditText) view.findViewById(R.id.editTextFPStockLimit);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void configureView(List<Product> products)
	{
		imageButtonFPselectImage.setOnClickListener(this);
		imageButtonFPselectImage.setTag(R.id.resourceDrawable, R.drawable.ic_insert_photo_black_24dp);
		imageButtonFPselectImage.setNextFocusRightId(editTextFPCategory.getId());
		editTextFPCategory.addTextChangedListener(this);
		editTextFPPrice.setOnEditorActionListener(this);
		editTextFPExpiration.setOnClickListener(this);
		adapterListViewFragmentProducts = new AdapterListViewFragmentProducts(activity, products);
		listViewFragmentProduct.setAdapter(adapterListViewFragmentProducts);
		listViewFragmentProduct.setOnItemClickListener(this);
		configuration.setProducts(adapterListViewFragmentProducts.getProducts());

		addFields();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.imageButtonFPselectImage:
				pickImage("Seleccionar Imagen", SELECT_PICTURE);

				break;
			case R.id.editTextFPExpiration:
				showDatePickerDialog(editTextFPExpiration);
		}
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		//parent.requestFocusFromTouch();
		selectedProduct = (Product) parent.getItemAtPosition(position);
		if(isItemSelected)
		{
			isItemSelected = false;
		} else {
			isItemSelected = true;
		}



		/*
		configuration.configureSharePreference();
		configuration.getPrefsEditor().putBoolean("isItemSelected", isItemSelected);
		configuration.getPrefsEditor().commit();
		*/
		//item.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
		//Product pro = products.get(position);
		//mListener.onFragmentProductSelected(pro);

		//view.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
		//view.setSelected(true);
		setSelectableChoiceConfig(view);
/*
		if (view.isSelected())
			view.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
		view.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));*/
	}

	public void setSelectableChoiceConfig(View view)
	{
		if (currentSelectedView != null && currentSelectedView != view)
		{
			unhighlightCurrentRow(currentSelectedView);
			currentSelectedView = view;
			highlightCurrentRow(currentSelectedView);
		}
		else if(currentSelectedView != null && currentSelectedView == view)
		{
			unhighlightCurrentRow(currentSelectedView);
			currentSelectedView = null;
			isItemSelected = false;
			selectedProduct = null;
		} else {
			currentSelectedView = view;
			highlightCurrentRow(currentSelectedView);
		}
	}

	private void unhighlightCurrentRow(View rowView)
	{
		if (rowView != null)
		{
			rowView.setBackgroundColor(Color.TRANSPARENT);
		}
		// TextView textView = (TextView) rowView.findViewById(R.id.menuTitle);
		// textView.setTextColor(getResources().getColor(R.color.white));
	}

	private void highlightCurrentRow(View rowView)
	{
		if (rowView != null)
		{
			rowView.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));
		}
		// TextView textView = (TextView) rowView.findViewById(R.id.menuTitle);
		// textView.setTextColor(getResources().getColor(R.color.yellow));

	}

	public void showDatePickerDialog(EditText editTextuttonSelected)
	{
		datePickerFragment = new DatePickerFragment();
		datePickerFragment.setIdEditTextSelected(editTextuttonSelected.getId());
		datePickerFragment.show(getFragmentManager(), "e");
		datePickerFragment.setOnDateSetListener(this);
	}


	@Override
	public void onDateSet(DatePicker view, int year, int month, int day)
	{


		String date = utility.createDateWithHyphen(year, month + 1, day);

		int idSelected = datePickerFragment.getIdSelected();
		if (idSelected == R.id.editTextFPExpiration)
		{
			//buttonSetStartDate.setVisibility(Button.GONE);
			editTextFPExpiration.setVisibility(EditText.VISIBLE);
//					LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
			//editTextFPExpiration.setLayoutParams(params);
			editTextFPExpiration.setText(date);

			editTextFPStock.requestFocus();
			//datePickerFragment.getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
			InputMethodManager inputManager = (InputMethodManager)  getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.SHOW_IMPLICIT);

		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus)
	{
		if (hasFocus)
		{
			switch (v.getId())
			{
				case R.id.editTextFPExpiration:
					showDatePickerDialog((EditText) v);
					break;
			}
		}
	}

	public void showSoftKeyboard(View view) {
		boolean focus= view.requestFocus();
		if(editTextFPCategory.onCheckIsTextEditor())
		{
			InputMethodManager inputMethodManager=(InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
		}

	}

	@Override
	public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent)
	{
		if(editTextFPPrice.getId() == textView.getId() && actionId == EditorInfo.IME_ACTION_NEXT)
		{
			showDatePickerDialog(editTextFPExpiration);
		}


		return false;
	}

	@Override
	public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
	{
		InputMethodManager inputMethodManager=(InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.showSoftInput(editTextFPCategory, InputMethodManager.SHOW_IMPLICIT);
	}

	@Override
	public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
	{

	}

	@Override
	public void afterTextChanged(Editable editable)
	{

	}


	public class AsyncTaskLoadImages extends AsyncTask<String, Void, String>
	{
		private ProgressDialog progressDialog;
		private Activity       activity;
		private String         imagePath;
		private ImageView      imageView;

		public AsyncTaskLoadImages(Activity activity)
		{
			this.activity = activity;
		}

		public AsyncTaskLoadImages()
		{

		}

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			progressDialog = new ProgressDialog(activity);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setMessage(activity.getResources().getString(R.string.downloading_images));
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... arg0)
		{
			//configureView(products);
			return "";
		}

		@Override
		protected void onPostExecute(String path)
		{
			super.onPostExecute(path);
			progressDialog.dismiss();

		}

	}

	private void configureTabHost(Bundle savedInstanceState)
	{
		tabs.setup();
		//  TAB 1
		String mostSold = getResources().getString(R.string.most_sold);
		spec = tabs.newTabSpec(mostSold);
		spec.setContent(R.id.tab1);

		LayoutInflater inflater      = activity.getLayoutInflater();
		View           viewMostSold  = inflater.inflate(R.layout.view_tabhost, null);
		TextView       titleMostSold = (TextView) viewMostSold.findViewById(R.id.textViewTitleTab);
		titleMostSold.setText(mostSold);
		FrameLayout frameImageMostSold = (FrameLayout) viewMostSold.findViewById(R.id.frame_image_tabhost);
		frameImageMostSold.setBackground(getResources().getDrawable(R.drawable.ic_drawer));
		spec.setIndicator(viewMostSold);


		tabs.addTab(spec);
		//  TAB 2
		String categories = getResources().getString(R.string.categories);
		spec = tabs.newTabSpec(categories);
		spec.setContent(R.id.tab2);


		View     viewCategories  = inflater.inflate(R.layout.view_tabhost, null);
		TextView titleCategories = (TextView) viewCategories.findViewById(R.id.textViewTitleTab);
		titleCategories.setText(categories);
		FrameLayout frameImageCategories = (FrameLayout) viewCategories.findViewById(R.id.frame_image_tabhost);
		frameImageCategories.setBackground(getResources().getDrawable(R.drawable.ic_drawer));
		spec.setIndicator(viewCategories);
		tabs.addTab(spec);

		tabs.setCurrentTab(0);
	}

	private void configureTabChanges()
	{
		tabs.setOnTabChangedListener(this);
	}

	@Override
	public void onTabChanged(String tabId)
	{
	}

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(Uri uri)
	{
		if (mListener != null)
		{
			//mListener.onProductSelected(product);
		}
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		mListener = null;
	}

	@Override
	public void onResume()
	{
		//isItemSelected = configuration.getPrefs().getBoolean("isItemSelected", false);

		addFields();
		for (View view : fields)
		{
			configuration.configureSharePreference();

			if (view instanceof EditText)
			{
				EditText editText = (EditText) view;
				String textValue = configuration.getPrefs().getString(editText.getId() + "", "");
				editText.setText(textValue);
				configuration.getPrefsEditor().remove(editText.getId() + "").commit();
			}
			else if (view instanceof ImageButton)
			{
				ImageButton imageButton = (ImageButton) view;

				String selectedImagePath = configuration.getPrefs().getString(imageButton.getId() + "", "");
				if (selectedImagePath.length() > 0)
				{
					Bitmap bitmap = new BitmapManager().decodeSampledBitmapFromResource(selectedImagePath, 36, 48);
					configureImageButton(bitmap, imageButton, null);
				}
				configuration.getPrefsEditor().remove(imageButton.getId() + "").commit();
			}
		}
		super.onResume();
	}

	@Override
	public void onPause()
	{
		configuration.configureSharePreference();
		configuration.getPrefsEditor().putBoolean("isItemSelected", isItemSelected);
		configuration.getPrefsEditor().commit();

		if(fields != null && fields.size() > 0)
		{
			for (View view : fields)
			{
				if (view instanceof EditText)
				{
					EditText editText = (EditText) view;

					configuration.configureSharePreference();
					configuration.getPrefsEditor().putString(editText.getId() + "", editText.getText().toString());
					configuration.getPrefsEditor().commit();
				}
				else if (view instanceof ImageButton)
				{
					ImageButton imageButton = (ImageButton) view;
					Uri uri = (Uri) imageButton.getTag(R.id.imageURI);
					if(uri != null)
					{
						HashMap properties = ProductImageManager.getFileProperties(activity, uri);
						String selectedImagePath = properties.get("path").toString();
						configuration.configureSharePreference();
						configuration.getPrefsEditor().putString(imageButton.getId() + "", selectedImagePath);
						configuration.getPrefsEditor().commit();
					}
				}
			}
		}
		/*

		*/
		super.onPause();
		//linearLayoutCreateProduct.setVisibility(View.GONE);
	}

	private void addFields()
	{
		fields = new ArrayList<>();
		fields.add(editTextFPCategory);
		fields.add(editTextFPDescription);
		fields.add(editTextFPSupplier);
		fields.add(editTextFPCost);
		fields.add(editTextFPPrice);
		fields.add(editTextFPExpiration);
		fields.add(editTextFPStock);
		fields.add(editTextFPStockLimit);
		fields.add(imageButtonFPselectImage);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		try
		{
			if(selectedProduct != null)
				CachedBitmap.writeObject(activity, "selectedProduct", selectedProduct);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

/*
		outState.putBoolean("isItemSelected", isItemSelected);
		outState.putSerializable("selectedProduct", selectedProduct);
*/
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null)
		{
			try
			{
				//obtener el item seleccionado y marcarlo en la lista

				selectedProduct = CachedBitmap.readObject(activity, "selectedProduct");
				Configuration.setSelectedProduct(selectedProduct);
			}
			catch (ClassNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		switch (id)
		{
			case R.id.action_search:
				Log.e("e", "search");
				return true;
			case R.id.action_add:
				actionAdd();
				return true;
			case R.id.action_edit:
				actionEdit();
				return true;
			case R.id.action_validate:
				actionValidate();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void actionAdd()
	{
		Log.e("e", "action add");
		if(isItemSelected)
		{
			mListener.onFragmentProductSelected(selectedProduct);
		}
		else {
			changeVisibility(linearLayoutCreateProduct);
		}
	}
	private void actionEdit()
	{
		if (selectedProduct != null && linearLayoutCreateProduct != null)
		{
			editProduct(selectedProduct, linearLayoutCreateProduct);
		}
		else
		{
			Log.e("Null", "selectedProduct or linearLayoutCreateProduct ");
			// Controlar cuando es 0, advertir o hacer algo
		}
	}

	private void editProduct(Product selectedProduct, LinearLayout linearLayout)
	{
		changeVisibility(linearLayout);

		editTextFPCategory.setText(selectedProduct.getCategory());
		editTextFPDescription.setText(selectedProduct.getDescription());
		editTextFPSupplier.setText(selectedProduct.getSupplierId());
		editTextFPCost.setText(selectedProduct.getCost());
		editTextFPPrice.setText(selectedProduct.getPrice());
		editTextFPExpiration.setText(selectedProduct.getExpireDate());
		editTextFPStock.setText(selectedProduct.getStock());
		editTextFPStockLimit.setText(selectedProduct.getLowLimitStock());

		if (selectedProduct.getBitmap() != null)
		{
			Bitmap bitmap = selectedProduct.getBitmap();
			Uri uri = (Uri) imageButtonFPselectImage.getTag();
			configureImageButton(bitmap, imageButtonFPselectImage, uri);
		}
		else
		{
			Log.e("null", "selectedProduct.getBitmap()");
		}
		updateEditingStatus();
	}

	private void actionValidate()
	{
		Log.e("e", "action VALIDATE");
		int createOrUpdateProcess;
		if (updateEditingStatus())
		{
			createOrUpdateProcess = PRODUCT_UPDATE;
		}
		else if(!areEmptyFields(fields))
		{
			createOrUpdateProcess = PRODUCT_CREATE;
		} else
		{
			Toast.makeText(activity, "Debe llenar los campos para registrar o actualizar", Toast.LENGTH_LONG).show();
			return;
		}
		createOrUpdateProduct(createOrUpdateProcess);
		updateEditingStatus();


	}


	private boolean updateEditingStatus()
	{
		//isItemSelected = configuration.getPrefs().getBoolean("isItemSelected", false);
		if (isItemSelected && !areEmptyFields(fields))
		{
			isEditingProduct = true;
		}
		else
		{
			isEditingProduct = false;
			/*
			configuration.configureSharePreference();
			configuration.getPrefsEditor().putBoolean("isItemSelected", isItemSelected);
			configuration.getPrefsEditor().commit();
			*/
			//Log.e("boolean", "isItemSelected: false");
			//Dialog for informing that "Debe seleccionar un producto para editarlo"
		}
		return isEditingProduct;
	}

	private void changeVisibility(LinearLayout linearLayout)
	{
		if (linearLayout.getVisibility() == View.VISIBLE)
		{
			linearLayout.setVisibility(View.GONE);
		}
		else
		{
			linearLayout.setVisibility(View.VISIBLE);
		}
	}

	private void createOrUpdateProduct(int createOrUpdateFlag)
	{
		if (!areEmptyFields(fields))
		{
			Product product = extractEntityFromFields();
			Uri selectedImageUri = (Uri) imageButtonFPselectImage.getTag(R.id.imageURI);

			if (createOrUpdateFlag == PRODUCT_CREATE)
			{
				product.setId(generateIdValue());
			}
			else if (createOrUpdateFlag == PRODUCT_UPDATE)
			{
				product.setId(selectedProduct.getId());
				product.setImagePath(selectedProduct.getImagePath());
			}
			saveEntityImageAndData(createOrUpdateFlag, product, selectedImageUri);
		}
		else
		{
			Toast.makeText(activity, "Complete los campos en blancos y la imagen para registrar el producto", Toast.LENGTH_LONG).show();
		}
	}

	private Product extractEntityFromFields()
	{
		Product product = new Product();
		Bitmap  bitmap  = ((BitmapDrawable) imageButtonFPselectImage.getDrawable()).getBitmap();
		product.setBitmap(bitmap);
		product.setCategory(editTextFPCategory.getText().toString());
		product.setDescription(editTextFPDescription.getText().toString());
		product.setSupplierId(editTextFPSupplier.getText().toString());
		product.setCost(editTextFPCost.getText().toString());
		product.setPrice(editTextFPPrice.getText().toString());
		product.setExpireDate(editTextFPExpiration.getText().toString());
		product.setStock(editTextFPStock.getText().toString());
		product.setLowLimitStock(editTextFPStockLimit.getText().toString());
		return product;
	}

	private void saveEntityImageAndData(int createOrUpdateFlag, Product product, Uri selectedImageUri)
	{
		String imagePath = ProductImageManager.generateImagePath(activity, product, selectedImageUri);

		//Creacion de nombre de la imagen a partir de la descripcion. Si no se ha cambiado la descripcion, se para al siguiente proceso
		if (product.getImagePath() != null && !imagePath.equalsIgnoreCase(product.getImagePath()))
		{
			if ( ProductImageManager.renameFile(product.getImagePath(), imagePath))
			{
				product.setImagePath(imagePath);
			}
		}
		else {
			product.setImagePath(imagePath);
		}

		if (ProductImageManager.saveToSDCard(activity, product))
		{
			long product_add_result = -1;
			if (createOrUpdateFlag == PRODUCT_CREATE)
			{
				product_add_result = productDatabase.addProduct(product);
			}
			else if (createOrUpdateFlag == PRODUCT_UPDATE)
			{
				product_add_result = productDatabase.updateProduct(product);
			}

			if (product_add_result == -1)
			{
				Toast.makeText(activity, "Error creating product", Toast.LENGTH_LONG).show();
			}
			else if (product_add_result == ProductDatabase.PRODUCT_ALREADY_EXISTS)
			{
				Toast.makeText(activity, "Product Already Exists", Toast.LENGTH_LONG).show();
			}
			else
			{
				products = (List<Product>) productDatabase.getAll();
				configureView(products);
				cleanFields(fields);
				linearLayoutCreateProduct.setVisibility(View.GONE);
			}
		}
		else
		{
			Log.e("null", "selectedProduct.getBitmap()");
		}
	}


	private String generateIdValue()
	{
		//	obtener el id anterior
		ProductDatabase productDatabase = new ProductDatabase(activity);
		String          idLastProduct   = productDatabase.getLastProductId().toString();
		String          idValue         = (Integer.valueOf(idLastProduct) + 1) + "";
		return idValue;
	}


	private boolean areEmptyFields(List<View> fields)
	{
		boolean isEmpty = false;
		for (View view : fields)
		{
			if (view instanceof EditText)
			{
				EditText editText = (EditText) view;
				if (editText.getText().toString().length() < 1)
				{
					isEmpty = true;
					break;
				}
			}
			else if (view instanceof ImageButton)
			{
				ImageButton imageButton = (ImageButton) view;
				try
				{
					int resource_drawable = (int) imageButton.getTag(R.id.resourceDrawable);
					if (resource_drawable == R.drawable.ic_insert_photo_black_24dp)
					{
						isEmpty = true;
						break;
					}
				}
				catch (Exception e)
				{
					e.getCause();
					break;
				}
			}
		}
		return isEmpty;
	}

	private void cleanFields(List<View> fields)
	{
		for (View view : fields)
		{
			if (view instanceof EditText)
			{
				EditText editText = (EditText) view;
				editText.setText("");
			}
			else if (view instanceof ImageButton)
			{
				ImageButton imageButton = (ImageButton) view;
				imageButton.setImageResource(R.drawable.ic_insert_photo_black_24dp);
				imageButton.setTag(null);
			}
		}
	}

	private void pickImage(String chooserTitle, int requestCode)
	{
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, chooserTitle), requestCode);

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode == Activity.RESULT_OK)
		{
			if (requestCode == SELECT_PICTURE)
			{
				try {
					Uri selectedImageUri = data.getData();
					Log.e("URI VAL", "selectedImageUri = " + selectedImageUri.toString());
					configureImageFromUri(selectedImageUri);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		showSoftKeyboard(editTextFPCategory);
	}

	private void configureImageFromUri(Uri selectedImageUri)
	{
		HashMap properties = ProductImageManager.getFileProperties(activity, selectedImageUri);
		if (properties != null)
		{
			selectedImagePath = properties.get("path").toString();
			if ( selectedImagePath != null)
			{
				// IF LOCAL IMAGE, NO MATTER IF ITS DIRECTLY FROM GALLERY (EXCEPT PICASSA ALBUM),
				// OR OI/ASTRO FILE MANAGER. EVEN DROPBOX IS SUPPORTED BY THIS BECAUSE DROPBOX DOWNLOAD THE IMAGE
				// IN THIS FORM - file:///storage/emulated/0/Android/data/com.dropbox.android/...
				System.out.println("local image");

				Bitmap bitmap = new BitmapManager().decodeSampledBitmapFromResource(selectedImagePath, 36, 48);
				if (bitmap != null)
				{
					configureImageButton(bitmap, imageButtonFPselectImage, selectedImageUri);
				}
			}
			else
			{
				Toast.makeText(activity, "Imagen No Valida", Toast.LENGTH_LONG).show();
			}
		}
		else
		{
			Toast.makeText(activity, "No se recuperaron los datos de la imagen", Toast.LENGTH_LONG).show();
		}

	}

	private void configureImageButton(Bitmap bitmap, ImageButton imageButton, Uri uri)
	{
		if (bitmap != null)
		{
			imageButton.setImageBitmap(bitmap);
			imageButton.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			imageButton.setTag(R.id.resourceDrawable, "ImageButton with real image now");
		}
		else
		{
			imageButton.setTag(R.id.resourceDrawable, R.drawable.ic_insert_photo_black_24dp);
		}

		if (uri != null)
		{
			imageButton.setTag(R.id.imageURI, uri);
		}

	}

	public void addToStock(Product product, int qunatityToAdd)
	{
		ProductDatabase productDatabase = new ProductDatabase(activity);
		product.setStock( (Integer.valueOf(product.getStock()) + qunatityToAdd) + "");
		productDatabase.updateProduct(product);
		Utility.fillupdateList(listViewFragmentProduct, adapterListViewFragmentProducts);
	}
	private final SearchView.OnQueryTextListener queryListener = new SearchView.OnQueryTextListener()
	{
		@Override
		public boolean onQueryTextSubmit(String query)
		{
			return true;
		}

		@Override
		public boolean onQueryTextChange(String query)
		{
			if (query != null)
			{
				EntitySearches entitySearch = new EntitySearches();
				List<Product> clients = new ArrayList<Product>();
				ProductDatabase productDatabase = new ProductDatabase(activity);
				products = (List<Product>) entitySearch.searchEntities(query, clients, productDatabase);
				configureView(products);
				return true;
			}
			else
			{
				return false;
			}
		}
	};


	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener
	{
		// TODO: Update argument type and name
		void onFragmentProductSelected(Product product);
	}
}
