package com.easypoint.app.easypoint.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.easypoint.app.easypoint.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductDatabase extends SQLiteOpenHelper implements SearchableDatabase
{

	public static final  String TABLE_NAME_PRODUCTS                 = "Products";
	public static final  String COLUMN_NAME_PRODUCT_ID              = "id";
	public static final  String COLUMN_NAME_PRODUCT_CATEGORY        = "category";
	public static final  String COLUMN_NAME_PRODUCT_DESCRIPTION     = "description";
	public static final  String COLUMN_NAME_PRODUCT_SUPPLIER        = "supplierId";
	public static final  String COLUMN_NAME_PRODUCT_NET_WEIGHT      = "netWeight";
	public static final  String COLUMN_NAME_PRODUCT_UNIT_TYPE       = "unitType";
	public static final  String COLUMN_NAME_PRODUCT_COST            = "cost";
	public static final  String COLUMN_NAME_PRODUCT_PRICE           = "price";
	public static final  String COLUMN_NAME_PRODUCT_EXPIRE_DATE     = "expireDate";
	public static final  String COLUMN_NAME_PRODUCT_STOCK           = "stock";
	public static final  String COLUMN_NAME_PRODUCT_LOW_LIMIT_STOCK = "lowLimitStock";
	public static final  String COLUMN_NAME_PRODUCT_IMAGE_PATH      = "imagePath";
	private static final int    DATABASE_VERSION                    = 1;
    public static final  int    PRODUCT_ALREADY_EXISTS                    = -2;

	private String createTable_products = "create table " +
	                                      TABLE_NAME_PRODUCTS + " ( " +
	                                      COLUMN_NAME_PRODUCT_ID + " integer primary key autoincrement , " +
	                                      COLUMN_NAME_PRODUCT_CATEGORY + " TEXT , " +
	                                      COLUMN_NAME_PRODUCT_DESCRIPTION + " TEXT , " +
	                                      COLUMN_NAME_PRODUCT_SUPPLIER + " TEXT , " +
	                                      COLUMN_NAME_PRODUCT_NET_WEIGHT + " TEXT, " +
	                                      COLUMN_NAME_PRODUCT_UNIT_TYPE  + " TEXT, " +
	                                      COLUMN_NAME_PRODUCT_COST + " TEXT, " +
	                                      COLUMN_NAME_PRODUCT_PRICE + " TEXT, " +
	                                      COLUMN_NAME_PRODUCT_EXPIRE_DATE + " TEXT , " +
	                                      COLUMN_NAME_PRODUCT_STOCK + " TEXT, " +
	                                      COLUMN_NAME_PRODUCT_LOW_LIMIT_STOCK + " TEXT, " +
	                                      COLUMN_NAME_PRODUCT_IMAGE_PATH + " TEXT )";

	public ProductDatabase(Context context)
	{
		super(context, TABLE_NAME_PRODUCTS, null, DATABASE_VERSION);
	}

	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL(createTable_products);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_PRODUCTS);
		db.execSQL(createTable_products);
	}

	public long addProduct(Product product)
	{	
		SQLiteDatabase db;
        if(!product.getDescription().trim().equalsIgnoreCase(getByName(product.getDescription()).toString().trim()))
        {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_NAME_PRODUCT_CATEGORY, product.getCategory());
            contentValues.put(COLUMN_NAME_PRODUCT_DESCRIPTION, product.getDescription());
            contentValues.put(COLUMN_NAME_PRODUCT_SUPPLIER, product.getSupplierId());
            contentValues.put(COLUMN_NAME_PRODUCT_NET_WEIGHT, product.getNetWeight() );
            contentValues.put(COLUMN_NAME_PRODUCT_UNIT_TYPE, product.getNetWeight() );
            contentValues.put(COLUMN_NAME_PRODUCT_COST, product.getCost() );
            contentValues.put(COLUMN_NAME_PRODUCT_PRICE, product.getPrice());
            contentValues.put(COLUMN_NAME_PRODUCT_EXPIRE_DATE, product.getExpireDate());
            contentValues.put(COLUMN_NAME_PRODUCT_STOCK, product.getStock() );
            contentValues.put(COLUMN_NAME_PRODUCT_LOW_LIMIT_STOCK, product.getLowLimitStock() );
            contentValues.put(COLUMN_NAME_PRODUCT_IMAGE_PATH, product.getImagePath());
            db = this.getWritableDatabase();
            long insertedID = db.insert(TABLE_NAME_PRODUCTS, null, contentValues);
            db.close();
            return insertedID;
        } else {
            return PRODUCT_ALREADY_EXISTS;
        }

	}
	
	public int updateProduct(Product product)
	{
		SQLiteDatabase db;
		db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME_PRODUCT_CATEGORY, product.getCategory());
        contentValues.put(COLUMN_NAME_PRODUCT_DESCRIPTION, product.getDescription());
        contentValues.put(COLUMN_NAME_PRODUCT_SUPPLIER, product.getSupplierId());
        contentValues.put(COLUMN_NAME_PRODUCT_NET_WEIGHT, product.getNetWeight() );
		contentValues.put(COLUMN_NAME_PRODUCT_UNIT_TYPE, product.getNetWeight() );
        contentValues.put(COLUMN_NAME_PRODUCT_COST, product.getCost() );
        contentValues.put(COLUMN_NAME_PRODUCT_PRICE, product.getPrice());
        contentValues.put(COLUMN_NAME_PRODUCT_EXPIRE_DATE, product.getExpireDate());
        contentValues.put(COLUMN_NAME_PRODUCT_STOCK, product.getStock() );
        contentValues.put(COLUMN_NAME_PRODUCT_LOW_LIMIT_STOCK, product.getLowLimitStock() );
        contentValues.put(COLUMN_NAME_PRODUCT_IMAGE_PATH, product.getImagePath());
		int ret = db.update(TABLE_NAME_PRODUCTS, contentValues, COLUMN_NAME_PRODUCT_ID + " = " + product.getId(), null);
		db.close();
		return ret;
	}

    public boolean updateProducts(List<Product> products)
    {
        List<Integer> responses = new ArrayList<Integer>();

        SQLiteDatabase db;
        db = this.getWritableDatabase();
        for(Product product : products)
        {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_NAME_PRODUCT_CATEGORY, product.getCategory());
            contentValues.put(COLUMN_NAME_PRODUCT_DESCRIPTION, product.getDescription());
            contentValues.put(COLUMN_NAME_PRODUCT_SUPPLIER, product.getSupplierId());
            contentValues.put(COLUMN_NAME_PRODUCT_NET_WEIGHT, product.getNetWeight() );
	        contentValues.put(COLUMN_NAME_PRODUCT_UNIT_TYPE, product.getNetWeight() );
            contentValues.put(COLUMN_NAME_PRODUCT_COST, product.getCost() );
            contentValues.put(COLUMN_NAME_PRODUCT_PRICE, product.getPrice());
            contentValues.put(COLUMN_NAME_PRODUCT_EXPIRE_DATE, product.getExpireDate());
            contentValues.put(COLUMN_NAME_PRODUCT_STOCK, product.getStock() );
            contentValues.put(COLUMN_NAME_PRODUCT_LOW_LIMIT_STOCK, product.getLowLimitStock() );
            contentValues.put(COLUMN_NAME_PRODUCT_IMAGE_PATH, product.getImagePath());
            int ret = db.update(TABLE_NAME_PRODUCTS, contentValues, COLUMN_NAME_PRODUCT_ID + " = " + product.getId(), null);
            responses.add(ret);
        }
        db.close();

        boolean successfullyUpdated = false;
        for(int response : responses)
        {
            if(response == -1)
            {
                return successfullyUpdated;
            }
        }
        successfullyUpdated = true;
        return successfullyUpdated;
    }

    public ArrayList<String> getImageNames(){

        SQLiteDatabase db = this.getWritableDatabase();
        String[] proyection =  new String[]
                {
                        COLUMN_NAME_PRODUCT_DESCRIPTION
                };

        Cursor cursor = db.query(TABLE_NAME_PRODUCTS, proyection, null, null, null, null, COLUMN_NAME_PRODUCT_ID);

        ArrayList<String> imageNames = new ArrayList<String>();

        if(cursor != null && cursor.getCount() != 0)
        {
            if(cursor.moveToFirst()){
                try
                {
                    do
                    {
                        String description = cursor.getString(0);
                        imageNames.add(  description);
                    }
                    while(cursor.moveToNext());

                }
                catch(Exception e )
                {
                    e.printStackTrace();
                }
            }
        }
        if(cursor != null)
        {
            cursor.close();
        }
        db.close();
        return imageNames;
    }

    public Product getProductById(String productID){

        SQLiteDatabase db = this.getWritableDatabase();
        String[] proyection =  new String[]
                {
                        COLUMN_NAME_PRODUCT_ID,
                        COLUMN_NAME_PRODUCT_CATEGORY,
                        COLUMN_NAME_PRODUCT_DESCRIPTION,
                        COLUMN_NAME_PRODUCT_SUPPLIER ,
                        COLUMN_NAME_PRODUCT_NET_WEIGHT,
                        COLUMN_NAME_PRODUCT_UNIT_TYPE,
                        COLUMN_NAME_PRODUCT_COST,
                        COLUMN_NAME_PRODUCT_PRICE,
                        COLUMN_NAME_PRODUCT_EXPIRE_DATE,
                        COLUMN_NAME_PRODUCT_STOCK,
                        COLUMN_NAME_PRODUCT_LOW_LIMIT_STOCK,
                        COLUMN_NAME_PRODUCT_IMAGE_PATH
                };

        Cursor cursor = db.query(TABLE_NAME_PRODUCTS, proyection, COLUMN_NAME_PRODUCT_ID + " = ?", new String[]{productID}, null, null, COLUMN_NAME_PRODUCT_ID);

        Product product = null;

        if(cursor != null && cursor.getCount() != 0)
        {
            if(cursor.moveToFirst()){
                try
                {
                    do
                    {
                        String id = cursor.getString(0);
                        String category= cursor.getString(1);
                        String description = cursor.getString(2);
                        String supplier = cursor.getString(3);
                        String netWeight = cursor.getString(    4);
	                    String unitType = cursor.getString(    5);
                        String cost = cursor.getString(         6);
                        String price = cursor.getString(        7);
                        String expireDate = cursor.getString(   8);
                        String stock = cursor.getString(        9);
                        String lowLimitStock = cursor.getString(10);
                        String imagePath = cursor.getString(    11);

                        product = new Product( id,  category,  description,  supplier,  netWeight, unitType, cost,  price,  expireDate,  stock,
		                        lowLimitStock,  imagePath);

                    }
                    while(cursor.moveToNext());

                }
                catch(Exception e )
                {
                    e.printStackTrace();
                }
            }
        }
        if(cursor != null)
        {
            cursor.close();
        }
        db.close();
        return product;
    }

	public  List<Product> getProductsFromCursor(Cursor cursor)
	{
		List<Product> products = new ArrayList<Product>();

		if(cursor != null && cursor.getCount() != 0)
		{
			if(cursor.moveToFirst()){
				try
				{
					do
					{
						String id = cursor.getString(0);
						String category= cursor.getString(1);
						String description = cursor.getString(2);
						String supplier = cursor.getString(3);
						String netWeight = cursor.getString(    4);
						String unitType = cursor.getString(    5);
						String cost = cursor.getString(         6);
						String price = cursor.getString(        7);
						String expireDate = cursor.getString(   8);
						String stock = cursor.getString(        9);
						String lowLimitStock = cursor.getString(10);
						String imagePath = cursor.getString(    11);

						products.add(  new Product( id,  category,  description,  supplier,  netWeight, unitType, cost,  price,  expireDate,  stock,
								lowLimitStock,  imagePath));
					}
					while(cursor.moveToNext());

				}
				catch(Exception e )
				{
					e.printStackTrace();
				}
			}
		}
        if(cursor != null)
        {
            cursor.close();
        }
		return products;
	}
	public int deleteProduct(int idProduct){
		SQLiteDatabase db;
		db = this.getWritableDatabase();
		int ret = db.delete(TABLE_NAME_PRODUCTS, COLUMN_NAME_PRODUCT_ID + " = " + idProduct,  null );
		db.close();
		return ret;
	}

    @Override
    public Object getByName(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] proyection =  new String[]
                {
                        COLUMN_NAME_PRODUCT_ID,
                        COLUMN_NAME_PRODUCT_CATEGORY,
                        COLUMN_NAME_PRODUCT_DESCRIPTION,
                        COLUMN_NAME_PRODUCT_SUPPLIER ,
                        COLUMN_NAME_PRODUCT_NET_WEIGHT,
                        COLUMN_NAME_PRODUCT_UNIT_TYPE,
                        COLUMN_NAME_PRODUCT_COST,
                        COLUMN_NAME_PRODUCT_PRICE,
                        COLUMN_NAME_PRODUCT_EXPIRE_DATE,
                        COLUMN_NAME_PRODUCT_STOCK,
                        COLUMN_NAME_PRODUCT_LOW_LIMIT_STOCK,
                        COLUMN_NAME_PRODUCT_IMAGE_PATH
                };

        Cursor cursor = db.query(TABLE_NAME_PRODUCTS, proyection, COLUMN_NAME_PRODUCT_DESCRIPTION + " like ?", new String[]{"%"+name+"%"}, null, null, COLUMN_NAME_PRODUCT_ID);
        List<Product> products = getProductsFromCursor(cursor);
        db.close();
        return products;
    }

    @Override
    public Object getAll()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] proyection =  new String[]
                {
                        COLUMN_NAME_PRODUCT_ID,
                        COLUMN_NAME_PRODUCT_CATEGORY,
                        COLUMN_NAME_PRODUCT_DESCRIPTION,
                        COLUMN_NAME_PRODUCT_SUPPLIER ,
                        COLUMN_NAME_PRODUCT_NET_WEIGHT,
                        COLUMN_NAME_PRODUCT_UNIT_TYPE,
                        COLUMN_NAME_PRODUCT_COST,
                        COLUMN_NAME_PRODUCT_PRICE,
                        COLUMN_NAME_PRODUCT_EXPIRE_DATE,
                        COLUMN_NAME_PRODUCT_STOCK,
                        COLUMN_NAME_PRODUCT_LOW_LIMIT_STOCK,
                        COLUMN_NAME_PRODUCT_IMAGE_PATH
                };

        Cursor cursor = db.query(TABLE_NAME_PRODUCTS, proyection, null, null, null, null, COLUMN_NAME_PRODUCT_ID);
        List<Product> products = getProductsFromCursor(cursor);
        db.close();
        return products;
    }

    public Object getLastProductId()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] proyection = new String[]
                {
                        COLUMN_NAME_PRODUCT_ID
                };
        String id = "";

        Cursor cursor = db.query(TABLE_NAME_PRODUCTS, proyection, null, null, null, null, null);
        if (cursor != null && cursor.getCount() != 0)
        {
            if (cursor.moveToLast())
            {
                try
                {
                    id = cursor.getString(0);
                    return id;
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if(cursor != null)
        {
            cursor.close();
        }
        if(db != null)
        {
            db.close();
        }
        return id;
    }


}
