package com.easypoint.app.easypoint.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.asynctasks.AsynTaskCheckImageSync;
import com.easypoint.app.easypoint.database.ClientDatabase;
import com.easypoint.app.easypoint.fragments.FragmentOrder;
import com.easypoint.app.easypoint.fragments.FragmentProduct;
import com.easypoint.app.easypoint.fragments.ProductSelection;
import com.easypoint.app.easypoint.fragments.FragmentSales;
import com.easypoint.app.easypoint.interfaces.PhaseHandler;
import com.easypoint.app.easypoint.model.Client;
import com.easypoint.app.easypoint.model.Product;
import com.easypoint.app.easypoint.process_management.AlertDialogManager;
import com.easypoint.app.easypoint.process_management.NumPad;
import com.easypoint.app.easypoint.util.Configuration;
import com.easypoint.app.easypoint.util.ConnectionDetector;
import com.easypoint.app.easypoint.util.EntitySearches;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity implements NavigationDrawerFragment.NavigationDrawerCallbacks,
                                                      FragmentOrder.OnFragmentInteractionListener, ProductSelection.OnFragmentInteractionListener,
                                                      FragmentSales.OnFragmentInteractionListener, FragmentProduct.OnFragmentInteractionListener
{

	/**
	 * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in {@link #restoreActionBar()}.
	 */
	private CharSequence  mTitle;
	private Configuration configuration;
	private Activity      activity;
	private Menu menu;
	private Product product;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.activity = MainActivity.this;

		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
			System.out.println("*** My thread is now configured to allow connection");
		}

		configuration = Configuration.getInstance(activity);
		SplashAsyncTask splashAsyncTask = new SplashAsyncTask(this);
		splashAsyncTask.execute();
	}

	public void initMainActivityComponents()
	{
		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();
		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
	}

	@Override
	public void onClientSelected(Client client)
	{
	}

	@Override
	public void onFragmentInteraction(Uri uri)
	{
	}

	@Override
	public void onProductSelected(Product product)
	{
		NumPad numPad = new NumPad();
		numPad.setHander(numPad.getHandlerNumPadOrders());
		this.product = product;
		numPad.setQuantityDialog(activity, product, handlerSetSalesProductQuantity);
	}

	private final PhaseHandler handlerSetSalesProductQuantity = new PhaseHandler()
	{
		@Override
		public void onBegan(Object oject)
		{
		}

		@Override
		public void onError(Object oject)
		{
		}

		@Override
		public void onCompleted(Object object)
		{
			int productQuantity = (int) object;
			if (productQuantity == 0)
			{
				new AlertDialogManager().showAlertDialog(activity, "Error", "Debe digitar una cantidad", false);
				//
			}
			else
			{

				FragmentManager fragmentManager = activity.getFragmentManager();
				FragmentOrder fragmentOrder = (FragmentOrder) fragmentManager.findFragmentById(R.id.main_frame);
				fragmentOrder.setProductQuantity(productQuantity, product);

			}
		}

		@Override
		public void onFeedback(Object oject)
		{
		}
	};

	@Override
	public void onFragmentProductSelected(Product product)
	{
		NumPad numPad = new NumPad();
		numPad.setHander(numPad.getHandlerNumPadInventory());
		this.product = product;
		numPad.setQuantityDialog(activity, product, handlerAddProductStock);
	}

	private final PhaseHandler handlerAddProductStock = new PhaseHandler()
	{
		@Override
		public void onBegan(Object oject)
		{
		}

		@Override
		public void onError(Object oject)
		{
		}

		@Override
		public void onCompleted(Object object)
		{
			int productQuantity = (int) object;
			if (productQuantity == 0)
			{
				new AlertDialogManager().showAlertDialog(activity, "Error", "Debe digitar una cantidad", false);
				//
			}
			else
			{
				FragmentManager fragmentManager = activity.getFragmentManager();
				FragmentProduct fragmentProduct = (FragmentProduct) fragmentManager.findFragmentById(R.id.main_frame);
				fragmentProduct.addToStock(product, productQuantity);
			}
		}

		@Override
		public void onFeedback(Object oject)
		{
		}
	};

	private class SplashAsyncTask extends AsyncTask<Void, Integer, Boolean>
	{

		private Activity           activity;
		private boolean            isConnected;
		private boolean            isGCMReady;
		public  ProgressDialog     progressDialog;
		private ConnectionDetector cd;
		private AlertDialogManager alert = new AlertDialogManager();

		private SplashAsyncTask(Activity activity)
		{
			super();
			this.activity = activity;
		}

		@Override
		protected void onPreExecute()
		{
			progressDialog = new ProgressDialog(activity);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setMessage(activity.getResources().getString(R.string.configuring_image_download));
			progressDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0)
		{
			isConnected = checkConnection();
			return isConnected;
		}

		public boolean checkConnection()
		{
			boolean isConnected = false;
			try
			{
				cd = new ConnectionDetector(getApplicationContext());

				if (cd.isConnectingToInternet())
				{
					isConnected = true;
				}

			}
			catch (Exception ie)
			{
				ie.printStackTrace();
			}

			return isConnected;
		}

		@Override
		protected void onPostExecute(Boolean isConnected)
		{
			progressDialog.dismiss();

			if (isConnected)
			{
				//check this flow
				initMainActivityComponents();

				AsynTaskCheckImageSync asynTaskCheckImageSync = new AsynTaskCheckImageSync(activity);
				asynTaskCheckImageSync.execute(Configuration.GET_IMAGE_QUANTITY);

			}
			else
			{
				//Toast.makeText(activity, "There is not internet connection", Toast.LENGTH_LONG).show();
				//alert.showAlertExit(activity, "Internet Connection Error", "Please connect to Internet", false);
				initMainActivityComponents();

				AsynTaskCheckImageSync asynTaskCheckImageSync = new AsynTaskCheckImageSync(activity);
				asynTaskCheckImageSync.execute(Configuration.GET_IMAGE_QUANTITY);

				//
			}

			//   trabajar offline solamente por el momento
			//initMainActivityComponents();
		}
	}

	@Override
	public void onNavigationDrawerItemSelected(int position)
	{
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
					   .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
					   .commit();
	}

	public void onSectionAttached(int number)
	{
		switch (number)
		{
			case 1:
				mTitle = getString(R.string.title_section1);
				FragmentOrder fragmentOrder = new FragmentOrder();
				Bundle bundle = new Bundle();
				//Serializable productsBundle = getArguments().getSerializable("products");
				//bundle.putSerializable("products", productsBundle);
				Configuration.setUpFragment(MainActivity.this, R.id.main_frame, getFragmentManager(), fragmentOrder, bundle);
				break;
			case 2:
				mTitle = getString(R.string.title_section2);
				FragmentProduct productFragment = new FragmentProduct();
				Bundle bundleProducts = new Bundle();
				//Serializable productsBundle = getArguments().getSerializable("products");
				//bundle.putSerializable("products", productsBundle);
				Configuration.setUpFragment(MainActivity.this, R.id.main_frame, getFragmentManager(), productFragment, bundleProducts);
				break;
			case 3:
				mTitle = getString(R.string.title_section3);

				FragmentSales sellFragment = new FragmentSales();
				Configuration.setUpFragment(MainActivity.this, R.id.main_frame, getFragmentManager(), sellFragment, null);
				break;
		}
	}


	public void restoreActionBar()
	{
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if (!mNavigationDrawerFragment.isDrawerOpen())
		{
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.

			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	/*
		@Override
		public boolean onOptionsItemSelected(MenuItem item)
		{
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.

			switch (item.getItemId()) {
				case R.id.action_search:
					return true;
				case R.id.action_add:
					Log.e("e", "Add");
					return true;
				default:
					return super.onOptionsItemSelected(item);
			}

		}
	*/
	private final SearchView.OnQueryTextListener queryListener = new SearchView.OnQueryTextListener()
	{
		@Override
		public boolean onQueryTextSubmit(String query)
		{
			return true;
		}

		@Override
		public boolean onQueryTextChange(String query)
		{
			if (query != null)
			{
				EntitySearches entitySearch = new EntitySearches();
				List<Client> clients = new ArrayList<Client>();
				ClientDatabase clientDatabase = new ClientDatabase(activity);
				clients = (List<Client>) entitySearch.searchEntities(query, clients, clientDatabase);

				return true;
			}
			else
			{
				return false;
			}
		}
	};


	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment
	{
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section
		 * number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber)
		{
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment()
		{
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState)
		{
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);
			return rootView;
		}

		@Override
		public void onAttach(Activity activity)
		{
			super.onAttach(activity);

			((MainActivity) activity).onSectionAttached(
					getArguments().getInt(ARG_SECTION_NUMBER));
		}
	}


}
