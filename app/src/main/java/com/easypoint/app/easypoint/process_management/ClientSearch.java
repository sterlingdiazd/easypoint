package com.easypoint.app.easypoint.process_management;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;


import com.easypoint.app.easypoint.fragments.FragmentOrder;
import com.easypoint.app.easypoint.interfaces.PhaseHandler;
import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.adapters.AdapterListViewClient;
import com.easypoint.app.easypoint.database.ClientDatabase;
import com.easypoint.app.easypoint.model.Client;
import com.easypoint.app.easypoint.util.EntitySearches;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sterlingdiazd on 6/1/2014.
 */
public class ClientSearch implements View.OnClickListener,
                                     TextWatcher
{
	private EditText              editTextCartClient;
	private Client                client;
	private Activity              activity;
	private ClientDatabase        clientDatabase;
	private AdapterListViewClient adapterListViewClient;
	private ListView              listViewClientList;
	private ImageButton           imageButtonAddClient;

	public Client getClient()
	{
		return client;
	}

	public Dialog clientListDialog(final Activity activity, final PhaseHandler handler)
	{
		this.activity = activity;
		clientDatabase = new ClientDatabase(activity);
		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
		alertDialogBuilder.setTitle("Clientes");
		//alertDialogBuilder.setIcon( );

		LayoutInflater inflater = LayoutInflater.from(activity);
		View rootView = inflater.inflate(R.layout.dialog_client_list, null);

		editTextCartClient = (EditText) rootView.findViewById(R.id.editTextCartClient);
		editTextCartClient.addTextChangedListener(this);
		imageButtonAddClient = (ImageButton) rootView.findViewById(R.id.imageButtonAddClient);
		imageButtonAddClient.setOnClickListener(this);

		listViewClientList = (ListView) rootView.findViewById(R.id.listViewClientList);
		adapterListViewClient = new AdapterListViewClient(activity);
        adapterListViewClient.setClients( (List<Client>) new ClientDatabase(activity).getAll());
        listViewClientList.setAdapter(adapterListViewClient);

        alertDialogBuilder.setView(rootView);

        final AlertDialog dialog = alertDialogBuilder.create();
        dialog.show();

        listViewClientList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                client = (Client) parent.getItemAtPosition(position);
				//handler.onCompleted(client);
	            android.app.FragmentManager fragmentManager = activity.getFragmentManager();
	            FragmentOrder fragmentOrder = (FragmentOrder) fragmentManager.findFragmentById(R.id.main_frame);
                fragmentOrder.setClient(client);
                dialog.dismiss();
            }
        });

        listViewClientList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                int idItem = position;
                client = (Client) parent.getItemAtPosition(position);
                new ClientManager(activity).setEditClientDialog(activity, client, listViewClientList, adapterListViewClient );

                return false;
            }
        });
        return dialog;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.imageButtonAddClient:

                new ClientManager(activity).setRegisterClientDialog(activity);
                break;


        }
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence textChanged, int start, int before, int count)
    {
        EntitySearches entitySearch = new EntitySearches();
        List<Client> clients = new ArrayList<Client>();
        ClientDatabase clientDatabase = new ClientDatabase(activity);
        clients = (List<Client>) entitySearch.searchEntities(textChanged.toString(), clients, clientDatabase);

        adapterListViewClient.setClients(clients);
        listViewClientList.setAdapter(adapterListViewClient);
        listViewClientList.refreshDrawableState();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

}
