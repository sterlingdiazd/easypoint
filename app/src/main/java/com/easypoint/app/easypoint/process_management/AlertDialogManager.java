package com.easypoint.app.easypoint.process_management;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.easypoint.app.easypoint.R;


public class AlertDialogManager {
	/**
	 * Function to display simple Alert Dialog
	 * @param activity - application context
	 * @param title - alert dialog title
	 * @param message - alert message
	 * @param status - success/failure (used to set icon)
	 * 				 - pass null if you don't want icon
	 * */

    public void showAlertDialog(final Activity activity, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		if(status != null)

        if(status)
        {
            alertDialog.setIcon(R.drawable.success);
        } else {
            alertDialog.setIcon(R.drawable.fail);
        }

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

    public void showAlertExit(final Activity activity, String title, String message, Boolean status)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        if(status != null)

            if(status)
            {
                alertDialog.setIcon(R.drawable.success);
            } else {
                alertDialog.setIcon(R.drawable.fail);
            }

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();

        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
        {
            public void onCancel(DialogInterface dialog) {
                activity.finish();
            }
        });
        // Setting Dialog Title


        // Setting Dialog Message



    }






}
