package com.easypoint.app.easypoint.fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.*;
import android.widget.*;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.adapters.AdapterListViewCart;
import com.easypoint.app.easypoint.database.ClientDatabase;
import com.easypoint.app.easypoint.database.ProductDatabase;
import com.easypoint.app.easypoint.interfaces.PhaseHandler;
import com.easypoint.app.easypoint.model.Client;
import com.easypoint.app.easypoint.model.Product;
import com.easypoint.app.easypoint.process_management.ClientOrders;
import com.easypoint.app.easypoint.process_management.ClientSearch;
import com.easypoint.app.easypoint.process_management.NumPad;
import com.easypoint.app.easypoint.process_management.OrderEdition;
import com.easypoint.app.easypoint.process_management.OrderManagement;
import com.easypoint.app.easypoint.util.Configuration;
import com.easypoint.app.easypoint.util.EntitySearches;
import com.easypoint.app.easypoint.util.Utility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentOrder.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentOrder#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentOrder extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, AdapterView.OnItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;

    private List<Product> cart;
    private TextView textViewOrderTotal, textViewClientName, textViewDebtAmount, textViewCurrentPurchaseAmount, textViewClientTotalDebt,
            textViewLabelCurrentPurchase, textViewClientTotal, textViewLabelClientDebt;
    private Button buttonRegisterOrder, buttonViewClientReport;
    private Activity activity;
    private Resources res;


    private ToggleButton toggleButtonPaymentMethod;
    private Product product;
    private NumPad numPad;
    private ImageButton imageButtonSearchClient;
    private static Configuration configuration;
    private static AdapterListViewCart adapterListViewCart;
    private static ListView listViewCart;
    private static ArrayList<String> imageNames;
    private Client client;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);

        Serializable productsBundle = getArguments().getSerializable("products");
        configureProductSelection(productsBundle);
    }

    private void configureProductSelection(Serializable productsBundle)
    {
        ProductSelection productSelection = new ProductSelection();
        Bundle bundle = null;
        if (productsBundle != null) {
            bundle = new Bundle();
            bundle.putSerializable("products", productsBundle);
        }
        configuration.setUpFragment(activity, R.id.dashboard_product_selection, getChildFragmentManager(), productSelection, bundle);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentOrder.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentOrder newInstance(String param1, String param2) {
        FragmentOrder fragment = new FragmentOrder();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentOrder() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        this.activity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        configuration = Configuration.getInstance(activity);
        initComponent(view);
		setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
		menu.clear();
        //super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);

        SearchView search = (SearchView) menu.findItem(R.id.action_search).getActionView();
        search.setQueryHint(getString(R.string.search_product));
        search.setOnQueryTextListener(queryListener);

        int id = search.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);

        TextView text = (TextView) search.findViewById(id);
        text.setHintTextColor(Color.WHITE);
    }

    private final SearchView.OnQueryTextListener queryListener = new SearchView.OnQueryTextListener()
    {
        @Override
        public boolean onQueryTextSubmit(String query)
        {
            return true;
        }

        @Override
        public boolean onQueryTextChange(String query)
        {
            if (query != null)
            {
                EntitySearches entitySearch = new EntitySearches();
                List<Product> clients = new ArrayList<Product>();
                ProductDatabase productDatabase = new ProductDatabase(activity);

                List<Product> products = (List<Product>) entitySearch.searchEntities(query, clients, productDatabase);
                Serializable productSearched = (Serializable) products;
                configureProductSelection(productSearched);
                return true;
            }
            else
            {
                return false;
            }
        }
    };


    private void initComponent(View view)
    {
        try
        {
            cart = new ArrayList<Product>();

            textViewOrderTotal = (TextView) view.findViewById(R.id.textViewOrderTotal);

            textViewClientName = (TextView) view.findViewById(R.id.textViewClientName);
            textViewClientName.setOnClickListener(this);

            textViewDebtAmount = (TextView) view.findViewById(R.id.textViewDebtAmount);
            textViewCurrentPurchaseAmount = (TextView) view.findViewById(R.id.textViewCurrentPurchaseAmount);

            textViewClientTotal = (TextView) view.findViewById(R.id.textViewClientTotal);

            textViewClientTotalDebt = (TextView) view.findViewById(R.id.textViewClientTotalDebt);
            textViewLabelClientDebt = (TextView) view.findViewById(R.id.textViewLabelClientDebt);
            textViewLabelCurrentPurchase = (TextView) view.findViewById(R.id.textViewLabelCurrentPurchase);
/*
        imageButtonAddClient = (ImageButton) rootView.findViewById(R.id.imageButtonAddClient);
        imageButtonAddClient.setOnClickListener(this);
*/
            buttonRegisterOrder = (Button) view.findViewById(R.id.buttonRegisterOrder);
            buttonRegisterOrder.setOnClickListener(this);

            buttonViewClientReport = (Button) view.findViewById(R.id.buttonViewClientReport);
            buttonViewClientReport.setOnClickListener(this);

        /*
        imageButtonSearchClient  = (ImageButton) rootView.findViewById(R.id.imageButtonSearchClient);
        imageButtonSearchClient.setOnClickListener(this);
        */
            toggleButtonPaymentMethod = (ToggleButton) view.findViewById(R.id.toggleButtonPaymentMethod);
            toggleButtonPaymentMethod.setOnCheckedChangeListener(this);

            adapterListViewCart = new AdapterListViewCart(activity, cart);
            listViewCart = (ListView) view.findViewById(R.id.listViewCart);
            listViewCart.setAdapter(adapterListViewCart);
            listViewCart.setOnItemClickListener(this);


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private List<Product> handleIntent(Intent intent)
    {
        List<Product> products = null;
        if (Intent.ACTION_SEARCH.equals(intent.getAction()))
        {
            String query = intent.getStringExtra(SearchManager.QUERY);
            products = (List<Product>) new ProductDatabase(activity).getByName(query);
        }
        return products;
    }

    @Override
    public void onClick(View view)
    {

        switch (view.getId())
        {
            case R.id.buttonRegisterOrder:
                boolean successfulOperation = new OrderManagement().registerOrder(activity, toggleButtonPaymentMethod, cart, client);
                if (successfulOperation)
                {
                    clearDebtFields();
                    clearRegisterOrderFields();
                }
                break;
            case R.id.imageButtonSearchClient:
                new ClientSearch().clientListDialog(activity, handler);
                break;

            case R.id.editTextCartClient:
                break;

            case R.id.textViewClientName:
                new ClientSearch().clientListDialog(activity, handler);
                break;

            case R.id.buttonViewClientReport:
                new ClientOrders().clientOrdersDialog(activity, client);
                break;
        }
    }

	private final PhaseHandler handler = new PhaseHandler()
    {
        @Override
        public void onBegan(Object oject)
        {
        }

        @Override
        public void onError(Object oject)
        {
        }

        @Override
        public void onCompleted(Object object)
        {
            client = (Client) object;
        }

        @Override
        public void onFeedback(Object oject)
        {
        }
    };


    private void clearRegisterOrderFields()
    {
        textViewClientName.setText("");
        textViewOrderTotal.setText("");
        //editTextCartClient.setText("");
        cart.clear();
        //adapterListViewCart.setProducts(cart);
        listViewCart.refreshDrawableState();
    }



    private void updateClientDebts(String idClient, String currentDebt)
    {
        int pastDebts = calculateClientDebt(idClient);

        textViewDebtAmount.setText(pastDebts + "");
        textViewCurrentPurchaseAmount.setText(currentDebt);

        int totalDebt = 0;
        if (currentDebt.length() > 0)
        {
            totalDebt = pastDebts + Integer.valueOf(currentDebt);
        }
        textViewClientTotalDebt.setText(totalDebt + "");
    }

    private int calculateClientDebt(String idClient)
    {
        int clientDebt = new ClientDatabase(activity).calculateDebt(idClient);
        return clientDebt;
    }

    public void setProductQuantity(int productQuantity, Product product)
    {
        for (Product prod : cart)
        {
            if (prod.getId().equalsIgnoreCase(product.getId()))
            {
                int currentProductQuantity = Integer.valueOf(prod.getQuantity());
                prod.setQuantity((currentProductQuantity + productQuantity) + "");
                updateCart(cart, adapterListViewCart, listViewCart, textViewOrderTotal, client);
                return;
            }
        }

        product.setQuantity(productQuantity+"");
        cart.add(product);
        updateCart(cart, adapterListViewCart, listViewCart, textViewOrderTotal, client);
    }

    public void updateCart(List<Product> cart, AdapterListViewCart adapterListViewCart, ListView listViewCart, TextView textViewOrderTotal, Client client)
    {
        adapterListViewCart.setProducts(cart);
        Utility.fillupdateList(listViewCart, adapterListViewCart);
        String total = new NumPad().calculateTotal(cart) + "";
        textViewOrderTotal.setText(total);
        if (client != null) updateClientDebts(client.getId(), textViewOrderTotal.getText().toString());
    }

    public void setClient(Client client)
    {
        this.client = client;

        this.textViewClientName.setVisibility(View.VISIBLE);
        textViewLabelClientDebt.setVisibility(View.VISIBLE);
        textViewLabelCurrentPurchase.setVisibility(View.VISIBLE);
        textViewDebtAmount.setVisibility(View.VISIBLE);
        textViewCurrentPurchaseAmount.setVisibility(View.VISIBLE);
        textViewClientTotalDebt.setVisibility(View.VISIBLE);
        textViewClientTotal.setVisibility(View.VISIBLE);
        buttonViewClientReport.setVisibility(View.VISIBLE);

        textViewClientName.setText(client.getName());

        if (client != null) updateClientDebts(client.getId(), textViewOrderTotal.getText().toString());
    }

    ////
    ////
    ///         FOR FRAGMENT
    ////
    ////

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            new ClientSearch().
                    clientListDialog(activity, handler).show();
        } else {
            clearDebtFields();
        }
    }

    private void clearDebtFields() {
        client = null;
        toggleButtonPaymentMethod.setChecked(false);

        textViewClientName.setText("");
        textViewDebtAmount.setText("");
        textViewCurrentPurchaseAmount.setText("");
        textViewClientTotalDebt.setText("");

        textViewClientName.setVisibility(View.INVISIBLE);
        textViewLabelClientDebt.setVisibility(View.INVISIBLE);
        textViewLabelCurrentPurchase.setVisibility(View.INVISIBLE);
        textViewClientTotal.setVisibility(View.INVISIBLE);
        buttonViewClientReport.setVisibility(View.INVISIBLE);

    }

    public Client getClient() {
        return client;
    }

    public TextView getTextViewOrderTotal() {

        return textViewOrderTotal;
    }


    public static ListView getListViewCart() {

        return listViewCart;
    }

    public static AdapterListViewCart getAdapterListViewCart() {
        return adapterListViewCart;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        Product product = (Product) adapterView.getItemAtPosition(i);
        new OrderEdition(activity).setDialogOrderEdition(product, cart, false, this);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
