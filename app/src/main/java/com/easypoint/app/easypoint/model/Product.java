package com.easypoint.app.easypoint.model;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by sterlingdiazd on 5/22/2014.
 */
public class Product implements Serializable
{

    private String id;
    private String category;
    private String description;
    private String supplierId;
    private String netWeight;
    private String unitType;
    private String cost;
    private String price;
    private String expireDate;
    private String stock;
    private String lowLimitStock;
    private String imagePath;
    private transient Bitmap bitmap;

    public String getQuantity()
    {
        return quantity;
    }

    public void setQuantity(String quantity)
    {
        this.quantity = quantity;
    }

    private String quantity;

    public Product()
    {
    }

    public Product(String id, String category, String description, String supplierId, String netWeight, String unitType, String cost,
			String price, String expireDate, String stock, String lowLimitStock, String imagePath)
	{
		this.id = id;
		this.category = category;
		this.description = description;
		this.supplierId = supplierId;
		this.netWeight = netWeight;
		this.unitType = unitType;
		this.cost = cost;
		this.price = price;
		this.expireDate = expireDate;
		this.stock = stock;
		this.lowLimitStock = lowLimitStock;
		this.imagePath = imagePath;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getLowLimitStock() {
        return lowLimitStock;
    }

    public void setLowLimitStock(String lowLimitStock) {
        this.lowLimitStock = lowLimitStock;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Bitmap getBitmap()
    {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap)
    {
        this.bitmap = bitmap;
    }

    public String getUnitType()
    {
        return unitType;
    }

    public void setUnitType(String unitType)
    {
        this.unitType = unitType;
    }
}
