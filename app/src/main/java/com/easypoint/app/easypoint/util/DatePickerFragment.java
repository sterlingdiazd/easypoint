package com.easypoint.app.easypoint.util;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.easypoint.app.easypoint.R;

public  class DatePickerFragment extends DialogFragment {

	private int year;
	private int month;
	private int day;
	private OnDateSetListener onDateSetListener;
	private int idButtonSelected;

	@Override
	public Dialog getDialog()
	{
		return dialog;
	}

	private Dialog dialog;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{

		final Calendar c     = Calendar.getInstance();
		int            year  = c.get(Calendar.YEAR);
		int            month = c.get(Calendar.MONTH);
		int            day   = c.get(Calendar.DAY_OF_MONTH);
		dialog = new DatePickerDialog(getActivity(), onDateSetListener, year,
									  month, day);
		;
		return dialog;
	}

	public void setOnDateSetListener(OnDateSetListener onDateSetListener)
	{
		this.onDateSetListener = onDateSetListener;
	}


	public void setIdEditTextSelected(int id)
	{
		this.idButtonSelected = id;

	}


	public int getIdSelected()
	{
		return idButtonSelected;
	}

}
