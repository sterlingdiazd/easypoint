package com.easypoint.app.easypoint.process_management;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.fragments.FragmentOrder;
import com.easypoint.app.easypoint.interfaces.NumPadInterface;
import com.easypoint.app.easypoint.interfaces.PhaseHandler;
import com.easypoint.app.easypoint.model.Product;

import java.util.List;


/**
 * Created by sterlingdiazd on 5/24/2014.
 */
public class NumPad implements View.OnClickListener
{

	private Button      Button1;
	private Button      Button2;
	private Button      Button3;
	private Button      Button4;
	private Button      Button5;
	private Button      Button6;
	private Button      Button7;
	private Button      Button8;
	private Button      Button9;
	private Button      ButtonDot;
	private Button      ButtonZero;
	private ImageButton imageButtonBackspace;
	private EditText    editTextNumberScreen;
	private Integer     productQuantity;
	private Product product;
	private  AlertDialog.Builder alertDialogBuilder;
	public AlertDialog outerDialog;
	private View           view;
	private NumPadInterface hander;

	public NumPadInterface getHander()
	{
		return hander;
	}

	public void setHander(NumPadInterface hander)
	{
		this.hander = hander;
	}

	public void setQuantityDialog(final Activity activity, Product product, final PhaseHandler handler)
	{
		alertDialogBuilder = new AlertDialog.Builder(activity);
		alertDialogBuilder.setTitle("Cantidad");
		//alertDialogBuilder.setIcon(null);
		//alertDialogBuilder.setMessage( "");
		this.product = product;

		LayoutInflater inflater = LayoutInflater.from(activity);
		view     = inflater.inflate(R.layout.layout_num_pad, null);
		initNumPadComponents(view, activity);
		alertDialogBuilder.setView(view);

		alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{

			}
		});

		final AlertDialog dialog = alertDialogBuilder.create();
		dialog.show();
		//Overriding the handler immediately after show is probably a better approach than OnShowListener as described below
		dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				productQuantity = Integer.valueOf(editTextNumberScreen.getText().toString());
				dialog.dismiss();
				handler.onCompleted(productQuantity);

			}
		});
		outerDialog = dialog;
	}

	public View initNumPadComponents(View view, Activity activity)
	{
		editTextNumberScreen = (EditText) view.findViewById(R.id.editTextNumberScreen);
		Button1 = (Button) view.findViewById(R.id.Button1);
		Button2 = (Button) view.findViewById(R.id.Button2);
		Button3 = (Button) view.findViewById(R.id.Button3);
		Button4 = (Button) view.findViewById(R.id.Button4);
		Button5 = (Button) view.findViewById(R.id.Button5);
		Button6 = (Button) view.findViewById(R.id.Button6);
		Button7 = (Button) view.findViewById(R.id.Button7);
		Button8 = (Button) view.findViewById(R.id.Button8);
		Button9 = (Button) view.findViewById(R.id.Button9);
		ButtonDot = (Button) view.findViewById(R.id.ButtonDot);
		ButtonZero = (Button) view.findViewById(R.id.ButtonZero);
		imageButtonBackspace = (ImageButton) view.findViewById(R.id.imageButtonBackspace);

		Button1.setOnClickListener(this);
		Button2.setOnClickListener(this);
		Button3.setOnClickListener(this);
		Button4.setOnClickListener(this);
		Button5.setOnClickListener(this);
		Button6.setOnClickListener(this);
		Button7.setOnClickListener(this);
		Button8.setOnClickListener(this);
		Button9.setOnClickListener(this);
		//ButtonDot.setOnClickListener(this);
		ButtonZero.setOnClickListener(this);
		imageButtonBackspace.setOnClickListener(this);

		return view;
	}

	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.Button1:
				this.addNumber(Button1.getText().toString());
				break;
			case R.id.Button2:
				this.addNumber(Button2.getText().toString());
				break;
			case R.id.Button3:
				this.addNumber(Button3.getText().toString());
				break;
			case R.id.Button4:
				this.addNumber(Button4.getText().toString());
				break;
			case R.id.Button5:
				this.addNumber(Button5.getText().toString());
				break;
			case R.id.Button6:
				this.addNumber(Button6.getText().toString());
				break;
			case R.id.Button7:
				this.addNumber(Button7.getText().toString());
				break;
			case R.id.Button8:
				this.addNumber(Button8.getText().toString());
				break;
			case R.id.Button9:
				this.addNumber(Button9.getText().toString());
				break;
			case R.id.ButtonDot:
				this.addNumber(ButtonDot.getText().toString());
				break;
			case R.id.ButtonZero:
				this.addNumber(ButtonZero.getText().toString());
				break;
			case R.id.imageButtonBackspace:
				this.getHander().onBackSpace();



				break;
		}
	}

	public void addNumber(String number)
	{
		this.getHander().onAddNumber(number);
	}


	public NumPadInterface getHandlerNumPadOrders()
	{
		return handlerNumPadOrders;
	}

	public final NumPadInterface handlerNumPadOrders = new NumPadInterface()
	{
		@Override public void onAddNumber(Object number)
		{
			String finalNumber = editTextNumberScreen.getText().toString() + number.toString();
			if (Integer.valueOf(finalNumber) > Integer.valueOf(product.getStock()))
			{
				editTextNumberScreen.setTextColor(Color.RED);
				editTextNumberScreen.setText(finalNumber);
				outerDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
				Button1.setEnabled(false);
				Button2.setEnabled(false);
				Button3.setEnabled(false);
				Button4.setEnabled(false);
				Button5.setEnabled(false);
				Button6.setEnabled(false);
				Button7.setEnabled(false);
				Button8.setEnabled(false);
				Button9.setEnabled(false);
				ButtonZero.setEnabled(false);
				ButtonDot.setEnabled(false);
				outerDialog.setTitle("Solo hay " + product.getStock() + " " + product.getDescription() + " disponibles");
			}
			else
			{
				editTextNumberScreen.setText(finalNumber);
			}
		}

		@Override public void onBackSpace()
		{
			char[] array  = editTextNumberScreen.getText().toString().toCharArray();
			int    lenght = array.length;
			String cadena = "";
			for (int x = 0; x <= lenght - 1; x++)
			{
				if (cadena.length() != lenght - 1)
				{
					cadena = cadena + array[x];
				}
			}

			if (((cadena.length() <= 0) ? 0 : Integer.valueOf(cadena)) <= Integer.valueOf(product.getStock()))
			{
				editTextNumberScreen.setText(cadena);
				editTextNumberScreen.setTextColor(Color.BLACK);
				outerDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
				outerDialog.setTitle("Cantidad");
				Button1.setEnabled(true);
				Button2.setEnabled(true);
				Button3.setEnabled(true);
				Button4.setEnabled(true);
				Button5.setEnabled(true);
				Button6.setEnabled(true);
				Button7.setEnabled(true);
				Button8.setEnabled(true);
				Button9.setEnabled(true);
				ButtonZero.setEnabled(true);
				ButtonDot.setEnabled(true);
			}
		}
	};

	public NumPadInterface getHandlerNumPadInventory()
	{
		return handlerNumPadInventory;
	}

	public final NumPadInterface handlerNumPadInventory = new NumPadInterface()
	{
		@Override public void onAddNumber(Object number)
		{
			String finalNumber = editTextNumberScreen.getText().toString() + number.toString();
			editTextNumberScreen.setText(finalNumber);
			/*
			if (Integer.valueOf(finalNumber) > Integer.valueOf(product.getStock()))
			{
				editTextNumberScreen.setTextColor(Color.RED);
				editTextNumberScreen.setText(finalNumber);
				outerDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
				Button1.setEnabled(false);
				Button2.setEnabled(false);
				Button3.setEnabled(false);
				Button4.setEnabled(false);
				Button5.setEnabled(false);
				Button6.setEnabled(false);
				Button7.setEnabled(false);
				Button8.setEnabled(false);
				Button9.setEnabled(false);
				ButtonZero.setEnabled(false);
				ButtonDot.setEnabled(false);
				outerDialog.setTitle("Solo hay " + product.getStock() + " " + product.getDescription() + " disponibles");
			}
			else
			{
				editTextNumberScreen.setText(finalNumber);
			}
			*/


		}

		@Override public void onBackSpace()
		{
			char[] array  = editTextNumberScreen.getText().toString().toCharArray();
			int    lenght = array.length;
			String cadena = "";
			for (int x = 0; x <= lenght - 1; x++)
			{
				if (cadena.length() != lenght - 1)
				{
					cadena = cadena + array[x];
				}
			}

			if (((cadena.length() <= 0) ? 0 : Integer.valueOf(cadena)) <= Integer.valueOf(product.getStock()))
			{
				editTextNumberScreen.setText(cadena);
				/*
				editTextNumberScreen.setTextColor(Color.BLACK);
				outerDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
				outerDialog.setTitle("Cantidad");
				Button1.setEnabled(true);
				Button2.setEnabled(true);
				Button3.setEnabled(true);
				Button4.setEnabled(true);
				Button5.setEnabled(true);
				Button6.setEnabled(true);
				Button7.setEnabled(true);
				Button8.setEnabled(true);
				Button9.setEnabled(true);
				ButtonZero.setEnabled(true);
				ButtonDot.setEnabled(true);
				*/
			}
		}
	};

	public int calculateTotal(List<Product> cart)
	{
		int total = 0;
		for (Product product : cart)
		{
			if (product != null && product.getQuantity() != null && product.getPrice() != null)
			{
				total += (Integer.valueOf(product.getQuantity()) * Integer.valueOf(product.getPrice()));
			}
		}
		return total;
	}


	public Integer getProductQuantity()
	{
		return productQuantity;
	}

	public void setProductQuantity(Integer productQuantity)
	{
		this.productQuantity = productQuantity;
	}
}


