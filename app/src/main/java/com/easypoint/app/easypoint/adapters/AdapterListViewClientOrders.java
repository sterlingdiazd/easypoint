package com.easypoint.app.easypoint.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.easypoint.app.easypoint.R;
import com.easypoint.app.easypoint.database.OrderDatabase;
import com.easypoint.app.easypoint.database.ProductDatabase;
import com.easypoint.app.easypoint.model.Client;
import com.easypoint.app.easypoint.model.Order;
import com.easypoint.app.easypoint.model.OrderDetail;
import com.easypoint.app.easypoint.model.Product;
import com.easypoint.app.easypoint.util.BitmapManager;
import com.easypoint.app.easypoint.util.Configuration;
import com.easypoint.app.easypoint.util.ProductImageManager;

import java.util.ArrayList;
import java.util.List;

public class AdapterListViewClientOrders extends BaseAdapter
{

	private Activity          activity;
	private List<OrderDetail> orderDetails;

	public AdapterListViewClientOrders(Activity activity, List<OrderDetail> orderDetails)
	{
		this.activity = activity;
		Configuration.getInstance(activity).configureSharePreference();
		this.orderDetails = orderDetails;

	}





	public View getView(int position, View view, ViewGroup viewGroup)
    {
        LayoutInflater layoutInflater = (LayoutInflater)  activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = layoutInflater.inflate(R.layout.adapter_client_orders, null);

        TextView textViewAdapterClientOrdersDate = (TextView) rootView.findViewById(R.id.textViewAdapterClientOrdersDate);
        TextView textViewAdapterClientOrdersTime = (TextView) rootView.findViewById(R.id.textViewAdapterClientOrdersTime);
        TextView textViewAdapterClientOrdersProductName = (TextView) rootView.findViewById(R.id.textViewAdapterClientOrdersProductName);
        TextView textViewAdapterClientOrdersProductPrice = (TextView) rootView.findViewById(R.id.textViewAdapterClientOrdersProductPrice);
        TextView textViewAdapterClientOrdersProductQuantity = (TextView) rootView.findViewById(R.id.textViewAdapterClientOrdersProductQuantity);
        TextView textViewAdapterClientOrdersProductTotal = (TextView) rootView.findViewById(R.id.textViewAdapterClientOrdersProductTotal);

        ImageView imageViewAdapterClientOrdersProductImage = (ImageView) rootView.findViewById(R.id.imageViewAdapterClientOrdersProductImage);

	    if(orderDetails != null && orderDetails.size() > 0)
	    {
		    OrderDetail orderDetail = orderDetails.get(position);

		    Product product = new ProductDatabase(activity).getProductById(orderDetail.getIdProduct());
		    OrderDatabase orderDatabase =new OrderDatabase(activity);

		    Order order = (orderDatabase != null) ? orderDatabase.getOrderByID(orderDetail.getIdOrder()) : null;

		    String[] datetime = order.getOrderDateTime().split(" ");
		    textViewAdapterClientOrdersDate.setText(datetime[0]);
		    textViewAdapterClientOrdersTime.setText(datetime[1] + " " + datetime[2]);

		    textViewAdapterClientOrdersProductName.setText(product.getDescription());

		    String imagePath = activity.getFilesDir() + "/" + Configuration.IMAGE_DIRECTORY + "/" +  product.getImagePath();

		    Bitmap bitmap = new BitmapManager().decodeSampledBitmapFromResource(imagePath, 96, 128);
		    if(bitmap != null)
		    {
			    imageViewAdapterClientOrdersProductImage.setImageBitmap(bitmap);
		    } else {
			    imageViewAdapterClientOrdersProductImage.setImageResource(R.drawable.ic_launcher);
		    }
		    imageViewAdapterClientOrdersProductImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

		    String price = orderDetail.getUnitPrice();
		    String quantity = orderDetail.getQuantity();

		    textViewAdapterClientOrdersProductPrice.setText(price);
		    textViewAdapterClientOrdersProductQuantity.setText(quantity);

		    String total = (Integer.valueOf(price) * Integer.valueOf(quantity)) + "";

		    textViewAdapterClientOrdersProductTotal.setText(total);
	    } else {
		    Log.e("e", "order detail is null");
	    }


        return rootView;
    }

	public void setOrderDetails(List<OrderDetail> orderDetails)
	{
		this.orderDetails = orderDetails;
	}

    public int getCount()
    {
        return orderDetails.size();
    }

    public Object getItem(int position)
    {
	    Object item = null;
	    if(orderDetails != null)
	    {
		    item = orderDetails.get(position);
	    }
        return item;
    }

    public long getItemId(int id) {
        return id;
    }



}
